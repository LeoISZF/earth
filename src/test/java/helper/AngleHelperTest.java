/*
 * ISTP, Irkutsk
 */
package helper;

import static helper.AngleHelper.*;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Lev Broman
 */
public class AngleHelperTest {
    
    public AngleHelperTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of thetaToLat method, of class AngleHelper.
     */
    @Test
    public void testThetaToLat() {
        System.out.println("TEST thetaToLat");
        double theta = 3.1415;
        double expResult = -90;
        double result = thetaToLat(theta);
        assertEquals(expResult, result, 0.01);

    }
//
//    /**
//     * Test of latToTheta method, of class AngleHelper.
//     */
//    @Test
//    public void testLatToTheta() {
//        System.out.println("latToTheta");
//        double lat = 0.0;
//        double expResult = 0.0;
//        double result = AngleHelper.latToTheta(lat);
//        assertEquals(expResult, result, 0.0);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of latToColat method, of class AngleHelper.
//     */
//    @Test
//    public void testLatToColat() {
//        System.out.println("latToColat");
//        double lat = 0.0;
//        double expResult = 0.0;
//        double result = AngleHelper.latToColat(lat);
//        assertEquals(expResult, result, 0.0);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of colatToLat method, of class AngleHelper.
//     */
//    @Test
//    public void testColatToLat() {
//        System.out.println("colatToLat");
//        double colat = 0.0;
//        double expResult = 0.0;
//        double result = AngleHelper.colatToLat(colat);
//        assertEquals(expResult, result, 0.0);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of colatToTheta method, of class AngleHelper.
//     */
//    @Test
//    public void testColatToTheta() {
//        System.out.println("colatToTheta");
//        double colat = 0.0;
//        double expResult = 0.0;
//        double result = AngleHelper.colatToTheta(colat);
//        assertEquals(expResult, result, 0.0);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of isOutOfCircle method, of class AngleHelper.
//     */
//    @Test
//    public void testIsOutOfCircle() {
//        System.out.println("isOutOfCircle");
//        double val = 0.0;
//        double expResult = 0.0;
//        double result = AngleHelper.isOutOfCircle(val);
//        assertEquals(expResult, result, 0.0);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of isValue2pi method, of class AngleHelper.
//     */
//    @Test
//    public void testIsValue2pi() {
//        System.out.println("isValue2pi");
//        double v = 0.0;
//        double expResult = 0.0;
//        double result = AngleHelper.isValue2pi(v);
//        assertEquals(expResult, result, 0.0);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of OrientTriangle method, of class AngleHelper.
//     */
//    @Test
//    public void testOrientTriangle() {
//        System.out.println("OrientTriangle");
//        double lambda = 0.0;
//        double poleLambda = 0.0;
//        boolean expResult = false;
//        boolean result = AngleHelper.OrientTriangle(lambda, poleLambda);
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }

    /**
     * Test of TriangleNewOrientation method, of class AngleHelper.
     */
    @Test
    public void testTriangleNewOrientation() {
        System.out.println("Test TriangleNewOrientation");
        double lambda = 3;
        double poleLambda = 3;
        boolean expResult = false;
        boolean result = TriangleNewOrientation(lambda, poleLambda);
        assertEquals(expResult, result);
       
    }

    /**
     * Test of isValidColat method, of class AngleHelper.
     */
    @Test
    public void testIsValidColat() {
        System.out.println("Test isValidColat");
        double colat = -5;
        boolean expResult = false;
        boolean result =isValidColat(colat);
        assertEquals(expResult, result);
    }
//
//    /**
//     * Test of isValidTheta method, of class AngleHelper.
//     */
//    @Test
//    public void testIsValidTheta() {
//        System.out.println("isValidTheta");
//        double theta = 0.0;
//        boolean expResult = false;
//        boolean result = AngleHelper.isValidTheta(theta);
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of isValidLambda method, of class AngleHelper.
//     */
//    @Test
//    public void testIsValidLambda() {
//        System.out.println("isValidLambda");
//        double lambda = 0.0;
//        boolean expResult = false;
//        boolean result = AngleHelper.isValidLambda(lambda);
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of check method, of class AngleHelper.
//     */
//    @Test
//    public void testCheck() {
//        System.out.println("check");
//        boolean x = false;
//        AngleHelper.check(x);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of lonToLambda method, of class AngleHelper.
//     */
//    @Test
//    public void testLonToLambda() {
//        System.out.println("lonToLambda");
//        double lon = 0.0;
//        double expResult = 0.0;
//        double result = AngleHelper.lonToLambda(lon);
//        assertEquals(expResult, result, 0.0);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of lambdaToLon method, of class AngleHelper.
//     */
//    @Test
//    public void testLambdaToLon() {
//        System.out.println("lambdaToLon");
//        double lambda = 0.0;
//        double expResult = 0.0;
//        double result = AngleHelper.lambdaToLon(lambda);
//        assertEquals(expResult, result, 0.0);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of colatToThetaArray method, of class AngleHelper.
//     */
//    @Test
//    public void testColatToThetaArray() {
//        System.out.println("colatToThetaArray");
//        double[] massLat = null;
//        double[] expResult = null;
//        double[] result = AngleHelper.colatToThetaArray(massLat);
//        assertArrayEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
    
}
