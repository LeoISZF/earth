/*
 * ISTP, Irkutsk
 */
package domain;

import static java.lang.Math.toRadians;
import java.util.HashMap;
import java.util.Map;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import dao.BorderDao;
import dto.BorderDto;
import static helper.AngleHelper.*;
import helper.ArrayHelper;

import java.util.List;

/**
 *
 * @author Lev Broman
 */
public class MagneticFluxTest {

    public MagneticFluxTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of magnetFlux method, of class MagneticFlux.
     */
    @Test
    public void testMagnetFluxFakeBorder15() {
        System.out.println("magnetFlux");
        double eps = 0.1;
        double[] border = getFakeBorder15();
        double expResult = 0.51;
        double result = MagneticFlux.magnetFluxInGW(border);
        assertEquals(expResult, result, eps);
    }

    /**
     * Test of magnetFlux method, of class MagneticFlux.
     */
    @Test
    public void testMagnetFluxFile() {
        System.out.println("magnetFluxFile");
        double eps = 0.1;
        List<BorderDto> borderList = new BorderDao().load();
        int n = borderList.size();
        double[] border = new double[n];
        for (int i = 0; i < n; i++) {
            border[i] = borderList.get(i).colat;
        }
        border = ArrayHelper.colatToTheta(border);
        
        double result = MagneticFlux.magnetFluxInGW(border);
        double expResult = 0.35;
        assertEquals(expResult, result, eps);
    }

    /**
     * Test of magnetFlux method, of class MagneticFlux.
     */
    @Test
    public void testMagnetFluxList() {
        System.out.println("magnetFlux");
        double eps = 0.1;
        Map<Double, Double> map = getTestCaseMap();
        for (Map.Entry<Double, Double> entry : map.entrySet()) {
            Double key = entry.getKey();
            Double value = entry.getValue();
            int lon = key.intValue();
            double[] border = getFakeBorder(lon);
            double expResult = value;
            double result = MagneticFlux.magnetFluxInGW(border);
            assertEquals(expResult, result, eps);
        }
    }

    private Map<Double, Double> getTestCaseMap() {
        Map<Double, Double> map = new HashMap<>();
        map.put(5.0, 0.06);
        map.put(10.0, 0.23);
        map.put(15.0, 0.51);
        map.put(20.0, 0.88);
        map.put(25.0, 1.35);
        map.put(30.0, 1.89);
        return map;
    }

    /**
     * Тест вычисления на кошироте чуть больше чем 15 
     * Test of magnetFlux method, of class MagneticFlux.
     */
    @Test
    public void testFluxFakeBorder15_20() {
        double[] border = getFakeBorder15_20();
        double result = MagneticFlux.magnetFluxInGW(border);
        assertTrue(result > 0.51);
        assertTrue(result < 0.88);
    }

    /**
     * Тест вычисления потока на кошироте чуть меньше чем 10
     * Test of magnetFlux method, of class MagneticFlux.
     */
    @Test
    public void testFluxFakeBorder10_5() {
        double[] border = getFakeBorder10_5();
        double result = MagneticFlux.magnetFluxInGW(border);
        assertTrue(result > 0.06);
        assertTrue(result < 0.23);
    }

    /**
     * Получить границу на кошироте 15 градусов с одной измененной коширотой , рад
     *
     * @return граница
     */
    private double[] getFakeBorder15_20() {
        double[] border = getFakeBorder(15);
        border[1] = toRadians(20);
        return border;
    }

    private double[] getFakeBorder10_5() {
        double[] border = getFakeBorder(10);
        border[3] = toRadians(5);
        return border;
    }

    /**
     * Получить границу на кошироте 15 градусов, рад
     *
     * @return граница
     */
    private double[] getFakeBorder15() {
        double[] border = getFakeBorder(15);
        return border;
    }

    /**
     * Получить границу на заданной кошироте, рад
     *
     * @param colat коширота задаваемая
     *
     * @return граница
     */
    private double[] getFakeBorder(int colat) {
        int dLon = 10;
        int n = 360 / dLon;
        double[] border = new double[n];
        for (int i = 0; i < n; i++) {
            border[i] = colat;
        }
        border = colatToThetaArray(border);
        return border;
    }
/**
 * Получить равномерную границу овала по кошироте от fromCoLat до toCoLat
 * @param fromColat коширота начальная минимальная  
 * @param toColat коширота максимальная конечная 
 * @return 
 */
    private double[] getEvenlyBorder(double fromColat, double toColat) {
        double[] border = new double[36];
        for (int i = 0; i < 36; i++) {
            border[i] = fromColat + (i + 1) * toColat / 36;
        }
        border = colatToThetaArray(border);
        return border;
    }

}
