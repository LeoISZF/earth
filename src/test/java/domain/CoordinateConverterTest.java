/*
 * ISTP, Irkutsk
 */
package domain;

import dao.PoleDao;
import dao.StationDao;
import static domain.CoordinateConverter.*;
import static helper.AngleHelper.*;
import static org.junit.Assert.*;
import static java.lang.Math.*;
import helper.ArrayHelper.*;
import helper.AngleHelper;
import java.util.Arrays;
import java.util.List;
import dto.StationDto;
import dto.PoleDto;
import org.junit.*;

/**
 *
 * @author Lev Broman
 */
public class CoordinateConverterTest {

    public CoordinateConverterTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of latToColat method, of class CoordinateConverter.
     */
    @Test

    public void testLatToColatEquator() {

        double lat = 0.0;
        double expResult = 90;
        double result = AngleHelper.latToColat(lat);
        assertEquals(expResult, result, 0.0);

    }

    @Test

    public void testLatToColatPole() {

        double lat = 0.0;
        double expResult = 90;
        double result = AngleHelper.latToColat(lat);
        assertEquals(expResult, result, 0.0);
    }

    @Test

    public void testIsOutOfCircle() {
        double eps = 0.001;
        int N = 10;
        double[] Array = new double[N];
        double[] Conv = new double[N];
        double[] PhiArr = new double[N];
        for (int i = 0; i < N; i += 1) {
            double z = (double) i * PI;
            Array[i] = -4 * PI + z / 2;
            PhiArr[i] = toDegrees(Array[i]);
            Conv[i] = isOutOfCircle(Array[i]);
            if (Array[i] % (2 * PI) == 0) {
                assertEquals(0, Conv[i], 0.01);
            }
        }
        double val = 2 * PI;
        double expResult = 0;
        double result = isOutOfCircle(val);
        assertEquals(expResult, result, eps);
    }

    /**
     * Test of colatToLat method, of class CoordinateConverter.
     */
    @Test
    public void testColatToLat() {
        System.out.println("colatToLatTest");
        double colat = 180;
        double expResult = -90;
        double result = colatToLat(colat);
        assertEquals(expResult, result, 0.0);
    }

    /**
     * Test of calcDipTheta,calcTheta,calcDipLambda,calcLambda methods, of class CoordinateConverter.
     *
     */
    @Test
    public void testGeoToDipTheta() {
        System.out.println("testGeoToDipTheta");
        List<PoleDto> poleList = new PoleDao().load();
        List<StationDto> stationList = new StationDao().load();
        double poleLat = poleList.get(0).dipNorthLat;
        double poleTheta = latToTheta(poleLat);
        double poleLon = poleList.get(0).dipNorthLon;
        double poleLambda = toRadians(poleLon);
        double theta = latToTheta(stationList.get(1).geoLat);
        double lambda = toRadians(stationList.get(1).geoLon);
        double dipTheta = calcDipTheta(poleTheta, poleLambda, theta, lambda);
        double dipColat = toDegrees(dipTheta);
        double dipLambda = calcnewDipLambda(poleTheta, poleLambda, theta, lambda, dipTheta);
        double theta1 = calcnewGeoTheta(dipTheta, dipLambda, poleTheta, poleLambda, lambda);
        double colat1 = toDegrees(theta1);
        double lambda1 = calcnewGeoLambda(poleLambda, poleTheta, dipTheta, dipLambda, theta);
        double expResult = calcDipTheta(poleTheta, poleLambda, theta1, lambda1);
        double result = calcDipTheta(poleTheta, poleLambda, theta, lambda);
        assertEquals(expResult, result, 0.05);
    }

    /**
     * Test of calcPsi method, of class CoordinateConverter.
     */
    @Test
    public void testCalcPsi() {
        System.out.println("Test Calc Psi");
        //irt
        double poleLat = new PoleDao().load().get(10).dipNorthLat;
        double poleTheta = latToTheta(poleLat);
        double poleLon = new PoleDao().load().get(10).dipNorthLon;
        double poleLambda = lonToLambda(poleLon);
        List<StationDto> stationsArr = new StationDao().load();
        for (int i = 0; i < 520; i++) {
            double theta = latToTheta(stationsArr.get(i).geoLat);
            double lambda = lonToLambda(stationsArr.get(i).geoLon);
            double dipTheta = calcDipTheta(poleTheta, poleLambda, theta, lambda);              
            double dipLambda=calcnewDipLambda(poleTheta, poleLambda, theta, lambda, dipTheta);
            

        double psi =calcPsi(poleTheta, poleLambda, theta, lambda, dipTheta);
        boolean orient=TriangleNewOrientation(lambda, poleLambda);
        if (orient){
            while (psi>0){
                System.out.println("Ошибка знака склонения");
        }            
        } else {
            while (psi<0) {                
                System.out.println("Ошибка знака склонения !");
            }
        }
//        assertEquals(expResult, result, 0.01);
        }

    }

    /**
     * Test of calcDipLambda method, of class CoordinateConverter.
     */
    @Test
    public void testCalcDipLambda() {

        double poleLat = new PoleDao().load().get(1).dipNorthLat;
        double poleTheta = toRadians(AngleHelper.latToColat(poleLat));
        double poleLon = new PoleDao().load().get(1).dipNorthLon;
        double get2 = new PoleDao().load().get(2).dipNorthLon;
        double poleLambda = toRadians(poleLon);
        double dipTheta = calcDipTheta(poleTheta, poleLambda, PI, 0);
        double theta = PI;
        double lambda = 0.0;
        double ddd = (cos(theta) - cos(dipTheta) * cos(poleTheta));
        double eee = (sin(dipTheta) * sin(poleTheta));
        double ak = (ddd / eee);
        double a = acos((cos(theta) - cos(dipTheta) * cos(poleTheta)) / (sin(dipTheta) * sin(poleTheta)));
        double expResult = 0.0;
        double result = CoordinateConverter.calcDipLambda(poleTheta, dipTheta, theta, lambda, poleLambda);
        assertEquals(expResult, result, 0.0001);
    }

    /**
     * Test of calcTheta method, of class CoordinateConverter.
     */
    @Test
    public void testCalcGeoTheta() {
        System.out.println("Test calcGeoTheta");

        for (int q = 0; q <= 360; q += 10) {//цикл для полюсов долгот
            for (int w = 0; w <= 180; w += 10) {//цикл для полюсов коширот
                double poleLon = q;
                double poleColat = w;
                double poleLambda = toRadians(poleLon);
                double poleTheta = colatToTheta(poleColat);
                double theta = toRadians(50);
                double lambda = toRadians(200);
                double dipTheta = calcDipTheta(poleTheta, poleLambda, theta, lambda);
                double dipLambda = calcnewDipLambda(poleTheta, poleLambda, theta, lambda, dipTheta);
                double result = calcnewGeoTheta(dipTheta, dipLambda, poleTheta, poleLambda, lambda);
                assertEquals(theta, result, 0.001);
            }
        }

    }

    /**
     * Test of calcGeoLambda method, of class CoordinateConverter.
     */
    @Test
    public void testCalcLambda() {
        System.out.println("calcLambda");
        double poleLambda = toRadians(5);
        double dipTheta = 0;
        double theta = toRadians(30);
        double poleTheta = toRadians(25);
        double dipLambda = toRadians(10);
        double expResult = toRadians(5);
        double result = calcnewGeoLambda(poleLambda, poleTheta, dipTheta, dipLambda, theta);
        assertEquals(expResult, result, 0.01);

    }

    /**
     * Test of lonToLambda, of class Angle Helper
     */
    @Test
    public void testLonToLambda() {
        double lon = 60;
        double lambda = lonToLambda(lon);
        double expResult = PI / 3;
        assertEquals(expResult, lambda, 0.000000001);
    }

    /**
     * Проверка соотвествия знаков разницы между долготами точки и полюса и разницы между пи и дипольной долготой
     * (lambda-poleLambda)==(PI-dipLambda)
     */
    @Test
    public void testZnak() {
        System.out.println("test znak");

        List<StationDto> Stations = new StationDao().load();
        for (int j = 0; j < 360; j += 10) {//цикл по долготе дип полюса 
            for (int k = 0; k < 180; k += 10) {//цикл по кошироте дип полюса 
                for (int it = 1; it < 10; it++) {
                    double poleLambda = lonToLambda(j);
                    double lambda = lonToLambda(Stations.get(it).geoLon);
                    double znak1 = lambda - poleLambda;
                    double poleTheta = colatToTheta(k);
                    double theta = latToTheta(Stations.get(it).geoLat);
                    double dipTheta = calcDipTheta(poleTheta, poleLambda, theta, lambda);
                    double dipLambda = calcnewDipLambda(poleTheta, poleLambda, theta, lambda, dipTheta);
                    double znak2 = PI - dipLambda;
                    //System.out.println("znak1="+znak1+", znak2="+znak2);
                    //знак lambda-poleLambda совпадает со знаком (pi-dipLambda)
                    if (znak1 > 0 && znak2 < 0 || znak1 < 0 && znak2 > 0) {
                        //System.out.println("несовпадение знаков");
                    }
                }
            }

        }

    }

    /**
     * Тест на проверку работы формул при изменении дипольного(южного магнитного) полюса
     *
     * @return
     */
    @Test
    public void testDiffDipPole() {
        List<StationDto> geoData = new StationDao().load();
        int N = 7;
        for (int q = 0; q < N; q++) {//цикл для полюсов долгот
            for (int w = 0; w < N; w++) {//цикл для полюсов коширот
                double poleLon = q * 60;
                double poleColat = w * 30;
                double poleLambda = toRadians(poleLon);
                double poleTheta = colatToTheta(poleColat);
                for (int index = 0; index < 528; index++) {//цикл для станций 
                    double theta = latToTheta(geoData.get(index).geoLat);
                    double lambda = toRadians(geoData.get(index).geoLon);
                    double dipTheta = calcDipTheta(poleTheta, poleLambda, theta, lambda);
                    double dipLambda = calcnewDipLambda(poleTheta, poleLambda, theta, lambda, dipTheta);
                    double newTheta = calcnewGeoTheta(dipTheta, dipLambda, poleTheta, poleLambda, lambda);
                    double newLambda = calcnewGeoLambda(poleLambda, poleTheta, dipTheta, dipLambda, newTheta);
                    assertEquals(theta, newTheta, 0.1);
                    assertEquals(lambda, newLambda, 0.1);
                }

            }

        }

        System.out.println("end testDiffDipPole ");
    }

    /**
     * 2ой Тест на проверку работы формул преобразования географических коорлинат в дипольные магнитные 
     * при изменении дипольного(южного магнитного) полюса
     *
     * @return
     */
    @Test
    public void test2DipPoleChanged() {
        List<StationDto> geoData = new StationDao().load();
        int N = 7;
        for (int dol = 0; dol <= 360; dol += 20) {//цикл для полюсов долгот
            for (int coshir = 0; coshir <= 180; coshir += 10) {//цикл для полюсов коширот
                double poleLon = dol;
                double poleColat = coshir;
                double poleLambda = lonToLambda(poleLon);
                double poleTheta = colatToTheta(poleColat);
                for (int index = 0; index < 528; index++) {//цикл для станций 
                    double theta = latToTheta(geoData.get(index).geoLat);
                    double lambda = toRadians(geoData.get(index).geoLon);
                    double dipTheta = calcDipTheta(poleTheta, poleLambda, theta, lambda);
                    double dipLambda = calcnewDipLambda(poleTheta, poleLambda, theta, lambda, dipTheta);
                    double newTheta = calcnewGeoTheta(dipTheta, dipLambda, poleTheta, poleLambda, lambda);
                    double newLambda = calcnewGeoLambda(poleLambda, poleTheta, dipTheta, dipLambda, newTheta);

                    assertEquals(theta, newTheta, 0.1);
                    assertEquals(lambda, newLambda, 0.1);
                }

            }

        }

        System.out.println("end test2DipPoleChanged ");
    }

    private double[][] setDipNorthPolesArray() {
        //шаг по сетке
        int step = 10;
        int N = 360 / step;
        int NArr = (N + 1) * (N + 1);//1369
        double[] poleLonArray = new double[N + 1];
        double[] poleColatArray = new double[N + 1];
        double[][] testArray = new double[NArr][2];
        for (int i = 0; i < N + 1; i++) {
            for (int j = 0; j < N + 1; j++) {
                poleLonArray[i] = i * step;
                poleColatArray[i] = i * step / 2;
                testArray[i * j][0] = poleLonArray[j];
                testArray[i * j][1] = poleColatArray[i];
            }
        }
        for (int k = 0; k < NArr; k++) {
            // System.out.println("pole lon=" + poleLonArray[i] + "  pole colat=" + poleColatArray[i]);
            System.out.println("array=" + testArray[k][0] + ", " + testArray[k][1]);
        }

        return testArray;
    }

    private double[][] setdipPolesArr() {
        int N = 7;
        double[][] Arr = new double[N][2];
        for (int i = 0; i < N; i++) {
            Arr[i][0] = i * 60;
            Arr[i][1] = i * 30;//коширота  
            System.out.println("Arr=" + Arr[i][0] + ", " + Arr[i][1]);
        }

        return Arr;
    }

}
