/*
 * ISTP, Irkutsk
 */
package app;

import java.io.*;

import static java.lang.Math.toRadians;
import service.PolarCapService;
import static domain.MagneticFlux.magnetFlux;
import  service.CoodinateConverterService;
import service.TempService;

/**
 *
 * @author Lev Broman
 */
public class GeogrToGeomagn {

    public static void main(String[] args) throws FileNotFoundException, IOException {
        new CoodinateConverterService().exec();        
        new PolarCapService().polarCapFlux(); 
        new PolarCapService().MagnetFluxFile();
        
       
       
    }
}
