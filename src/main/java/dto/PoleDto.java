/*
 * ISTP, Irkutsk
 */
package dto;

/**
 * Полюс
 *
 * @author Lev Broman
 */
public class PoleDto {

    /**
     * Год (эпоха)
     */
    public int year;
    /**
     * Широта дипольного полюса в ГСК, град
     */
    public double dipNorthLat;
    /**
     * Долгота дипольного полюса в ГСК, град
     */
    public double dipNorthLon;

    /**
     * Контструктор копирования
     *
     * @param pole полюса
     */
    public PoleDto(PoleDto pole) {
        this.dipNorthLat = pole.dipNorthLat;
        this.dipNorthLon = pole.dipNorthLon;
        this.year = pole.year;
    }

    /**
     * Конструктор по-умолчанию
     */
    public PoleDto() {

    }

}
