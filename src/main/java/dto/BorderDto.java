/*
 * ISTP, Irkutsk
 */
package dto;

/**
 *
 * @author Lev Broman
 */
public class BorderDto {
    /**
     * Долгота
     */
    public int lon;
    /**
     *  Коширота
     */
    public double colat; 
    
    /**
     * Контструктор копирования
     *
     * @param border граница интегрирования потока по кошироте
     */
    public BorderDto(BorderDto border) {
        this.lon = border.lon;
        this.colat = border.colat;
    }        
    
    /**
     * конструктор по умолчанию
     */
    public BorderDto() {
        
    }
    
    
}
