/*
 * ISTP, Irkutsk
 */
package dto;

/**
 * Станция в эпохе
 *
 * @author Lev Broman
 */
public class StationYearDto implements Comparable {

    /**
     * Станция в ГСК
     */
    public StationDto stationDto;
    /**
     * Полюс
     */
    public PoleDto poleDto;
    /**
     * Широта станции в ДСК, град
     */
    public double dipLat;
    /**
     * Долгота станция в ДСК, град
     */
    public double dipLon;
    /**
     * Дипольное магнитное склонения, град
     */
    public double declination;

    /**
     * Конструктор по-умолчанию
     */
    public StationYearDto() {

    }

    /**
     * Конструктор копирования
     *
     * @param stationYear станция в эпохе
     */
    public StationYearDto(StationYearDto stationYear) {
        this.stationDto = stationYear.stationDto;
        this.poleDto = stationYear.poleDto;
        this.dipLat = stationYear.dipLat;
        this.dipLon = stationYear.dipLon;
        this.declination = stationYear.declination;
        this.poleDto = new PoleDto(stationYear.poleDto);
        this.stationDto = new StationDto(stationYear.stationDto);
    }

    /**
     * 
     * Сортировка , вызываемая по умолчанию 
     * Сортирует по значению склонения от меньшего к большему
     * @param o
     * @return 
     */
    @Override
    public int compareTo(Object o) {
        if (o == null) {
            return 1;
        }
        StationYearDto x = (StationYearDto) o;
        if (x == this) {
            return 0;
        }
        if (this.declination == x.declination) {
            return 0;
        }
        if (this.declination < x.declination) {
            return -1;
        } else {
            return 1;
        }
    }
    
    @Override
    public String toString() {
        return "decl = " + this.declination;
    }

}
