/*
 * ISTP, Irkutsk
 */
package dto;

/**
 * Станция
 *
 * @author Lev Broman
 */
public class StationDto {

    /**
     * code код станции IAGA
     */
    public String code;
    /**
     * Широта станции в ГСК, градус
     */
    public double geoLat;
    /**
     * Долгота станции в ГСК, градус
     */
    public double geoLon;
    /**
     * Полное название станинции
     */
    public String fullName;

    /**
     * Конструктор копирования
     *
     * @param station станция
     */
    public StationDto(StationDto station) {
        this.code = station.code;
        this.geoLat = station.geoLat;
        this.geoLon = station.geoLon;
        this.fullName = station.fullName;
    }

    /**
     * Конструктор по-умолчанию
     */
    public StationDto() {

    }
}
