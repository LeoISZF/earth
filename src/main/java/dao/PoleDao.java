
/*
 * ISTP, Irkutsk
 */
package dao;

import dto.PoleDto;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Data access object для работы с файлом с полюсами
 *
 * @author Lev Broman
 */
public class PoleDao {

    private final String fileNameDefault = "poles.txt";
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    /**
     * Загрузить данные из файла с полюсами по-умолчанию
     *
     * @return список полюсов
     */
    public List<PoleDto> load() {
        return load(fileNameDefault);
    }

    /**
     * Загрузить данные из файла
     *
     * @param fileName имя файла с полюсами
     * @return список полюсов
     * Даны координаты полюса в западной долготе
     * условимся что западное направление считаем отрицательным
     */
    public List<PoleDto> load(String fileName) {

        List<PoleDto> result = new ArrayList<>();
        try (FileInputStream fstream = new FileInputStream(fileName)) {
            String strLine;
            BufferedReader br = new BufferedReader(new InputStreamReader(fstream));
            boolean isHeader = true;
            while ((strLine = br.readLine()) != null) {

                if (!isHeader) {
                    String[] tokens = strLine.split("\\s");
                    PoleDto pole = new PoleDto();
                    pole.year = Integer.parseInt(tokens[0]);
                    pole.dipNorthLat = Double.parseDouble(tokens[1]);
                    pole.dipNorthLon = Double.parseDouble(tokens[2]);
                    result.add(pole);
                    logger.debug(pole.year + " year readed ");
                } else {
                    isHeader = false;
                }
            }
        } catch (IOException ex) {
            String msg = "error poles file read " + fileName;
            logger.error(msg);
            throw new IllegalStateException("read poles error");
        }
        logger.info("end");
        return result;
    }

}
