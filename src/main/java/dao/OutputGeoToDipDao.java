/*
 * ISTP, Irkutsk
 */
package dao;

import domain.CoordinateConverter;
import static domain.CoordinateConverter.*;
import dto.PoleDto;
import dto.StationDto;
import dto.StationYearDto;
import static helper.StringHelper.repeat;
import java.io.FileWriter;
import java.io.IOException;
import static java.lang.Math.toDegrees;
import static java.lang.Math.toRadians;
import java.util.List;
import static org.junit.Assert.assertEquals;
import org.slf4j.LoggerFactory;
import helper.AngleHelper;
import static helper.AngleHelper.*;

/**
 * DAO для работы с результатами преобразования ДСК в ГСК, ГСК в ДСК
 * Data Access Layer(Object)
 * @author Lev Broman
 */
public class OutputGeoToDipDao {

    private final org.slf4j.Logger logger = LoggerFactory.getLogger(this.getClass());

    /**
     * Сохранить спискок с координатами станций в ДСК в различные эпохи в файл
     *
     * @param fileName имя файла
     * @param list список с координатами
     */
    public void saveDip(String fileName, List<StationYearDto> list) {
        String ff = "%13.2f";
        String sf = "%13s";
        String header = String.format(repeat(sf, 8) + "%s", "year", "code", "geoLat", "geoLon", "psi", " dipLat", " dipLon ", "city", "\r\n");
        try (FileWriter writer = new FileWriter(fileName, false)) {
            writer.write(header);
            for (StationYearDto dto : list) {
                String line = String.format("%5d" + sf + repeat(ff, 5) + "%33s" + "%s",
                        dto.poleDto.year,
                        dto.stationDto.code,
                        dto.stationDto.geoLat,
                        dto.stationDto.geoLon,
                        dto.declination,
                        dto.dipLat,
                        dto.dipLon,
                        dto.stationDto.fullName,
                        "\r\n");
                writer.write(line);
            }
            writer.close();
        } catch (IOException ex) {
            logger.error("error ouput in file" + fileName);
            System.out.println(ex.getMessage());
        }
    }

    /**
     * Сохранить списки с координатами станций для последующего визуального сравнения
     *
     * @param fileName имя файла
     * @param list список с координатами станций в различные эпохи
     * @param staionList список станций в ГСК
     */
    public void saveGeo(String fileName, List<StationYearDto> list, List<StationDto> staionList) {
        String ff = "%13.2f";
        String sf = "%13s";
        String header = String.format(repeat(sf, 5) + "%s", "theta", "lambda", "psi", "new theta", "new lambda", "\r\n");
        try (FileWriter writer = new FileWriter(fileName, false)) {
            writer.write(header);
            for (StationYearDto dto : list) {
                StationDto original = getStation(staionList, dto.stationDto.code);
                String line = String.format("%5d" + sf + repeat(ff, 5) + "%33s" + "%s",
                        dto.poleDto.year,
                        dto.stationDto.code,
                        dto.stationDto.geoLat,
                        dto.stationDto.geoLon,
                        dto.declination,
                        original.geoLat,
                        original.geoLon,
                        dto.stationDto.fullName,
                        "\r\n");
                writer.write(line);
            }
            writer.close();
        } catch (IOException ex) {
            logger.error("error ouput in file" + fileName);
            System.out.println(ex.getMessage());
        }
    }

    /**
     * Получить станцию по ее коду
     *
     * @param staionList список станций
     * @param code код станции
     * @return станция
     */
    private StationDto getStation(List<StationDto> staionList, String code) {
        for (StationDto station : staionList) {
            if (station.code.equals(code)) {
                return station;
            }
        }
        throw new IllegalArgumentException("station is not found");
    }

    /**
     * Сохранение и вывод в фаил координат станций, склонения,
     *
     * @param poleList список полюсов
     * @param stationList список станций
     */
    public void save(List<PoleDto> poleList, List<StationDto> stationList) {
        double lat;
        double colat;
        double poleLon, poleLat;
        double poleLambda, poleTheta;
        double geolat, geolon;
        double theta, lambda, dipTheta, dipLambda;
        double dipColat, dipLon, psi, psiDeg, lambda1, lon1, theta1, colat1, lat1;
        int num_of_stations = stationList.size();
        int num_of_poles = poleList.size();
        //f-float, s-string, d-int  "%nuber of all signs.after comma+format specifier"
        String ff = "%13.2f";
        String sf = "%13s";        
        String header = String.format(repeat(sf, 5) + "%s", "lat", "lon", "psi", "new lat", "new lon", "\r\n");
        //templates
        //String header = String.format(repeat(sf, 5) + "%s", "theta", "lambda","psi", "new theta", "new lambda", "\r\n");
        //String header = String.format(repeat(sf, 8) + "%s", "year", "code", "geoLat", "geoLon", "psi", " dipLat", " dipLon ", "fullName", "\r\n");
        String fileName = "notesNew.txt";
        try (FileWriter writer = new FileWriter(fileName, false)) {
            writer.write(header);
            for (PoleDto pole : poleList) {
                for (StationDto station : stationList) {
                    geolon = station.geoLon;
                    geolat = station.geoLat;
                    theta = toRadians(AngleHelper.latToColat(geolat));
                    lambda = toRadians(geolon);
                    poleLat = pole.dipNorthLat;
                    String stationCode = station.code;
                    String city = station.fullName;
                    int pole_year = pole.year;
                    poleLon = pole.dipNorthLon;
                    poleLon = (poleLon > 0) ? poleLon -= 360 : poleLon;
                    poleLambda = toRadians(poleLon);
                    poleTheta = toRadians(AngleHelper.latToColat(poleLat));
                    dipTheta = calcDipTheta(poleTheta, poleLambda, theta, lambda);
                    dipLambda = calcnewDipLambda(poleTheta, poleLambda, theta, lambda, dipTheta);
                    dipLon = lambdaToLon(dipLambda);
                    dipColat = toDegrees(dipTheta);
                    double dipLat = colatToLat(dipColat);
                    psi = calcPsi(poleTheta, poleLambda, theta, lambda, dipTheta);
                    psiDeg = toDegrees(psi);                    
                    theta1 = calcnewGeoTheta(dipTheta, dipLambda, poleTheta, poleLambda, lambda);                    
                    lambda1 = calcnewGeoLambda(poleLambda, poleTheta, dipTheta, dipLambda, theta1);
                    lon1 = toDegrees(lambda1);
                    colat1 = toDegrees(theta1);
                    lat1 = colatToLat(colat1);
                    colat = toDegrees(theta);
                    String line = String.format(repeat(ff, 5) + "%s", geolat, geolon, psi, lat1, lon1, "\r\n");
                    //templates
                    //String line2 = String.format(repeat(ff, 4) + "%s", colat, geolon, colat1, lon1, "\r\n");
                    //String line2 = String.format(repeat(ff, 5) + "%s", geolat, geolon, psi, lat1,lon1, "\r\n");
                    //String line3=String.format("%5d"+sf+repeat(ff,5)+"%20s"+"%s",pole_year,station_code, geolat,geolon,psiDeg,lat1,lon1,fullName,"\r\n");
                    //String line4 = String.format("%5d" + sf + repeat(ff, 5) + "%33s" + "%s", pole_year, stationCode, geolat, geolon, psiDeg, dipLat, dipLon, fullName, "\r\n");
                    writer.write(line);
                    assertEquals(geolat, lat1, 0.001);
                    assertEquals(geolon, lon1, 0.001);
                    assertEquals(theta, theta1, 0.001);
                    assertEquals(lambda, lambda1, 0.001);
                }
            }
            writer.close();
        } catch (IOException ex) {
            logger.error("error ouput in file" + fileName);
            throw new IllegalStateException(ex);
        }
    }
}
