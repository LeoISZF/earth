/*
 * ISTP, Irkutsk
 */
package dao;

import dto.BorderDto;
import java.io.*;

import java.util.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Lev Broman
 */
public class BorderDao {

    private final String fileName = "BorderFile.txt";
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    public List<BorderDto> load() {
        return load(fileName);
    }

    public List<BorderDto> load(String fileName) {      
       
        List<BorderDto> result = new ArrayList<>();
        try (FileInputStream fstream = new FileInputStream(fileName)) {
            String strLine;
            BufferedReader br = new BufferedReader(new InputStreamReader(fstream));
            boolean isHeader = true;            
            while ((strLine = br.readLine()) != null) {                
                if (!isHeader) {
                    BorderDto border = new BorderDto();
                    String[] tokens = strLine.split("\\s");
                    border.lon = Integer.parseInt(tokens[0]);
                    border.colat = Double.parseDouble(tokens[1]);
                    result.add(border);
                } else {
                    isHeader = false;
                }
            }
        } catch (IOException ex) {
            String msg = "error Border file read " + fileName;
            logger.error(msg);
            System.out.println(" exeption !");
            throw new IllegalStateException("read poles error");
        }
        return result;
    }

}
