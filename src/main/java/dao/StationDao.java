/*
 * ISTP, Irkutsk
 */
package dao;

import dto.StationDto;
import java.io.*;
import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Dao для работы с файлом станций
 *
 * @author Lev Broman
 */
public class StationDao {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    private final String fileNameDefault = "Stat.txt";

    /**
     * Загрузить данные из файла со станциями по-умолчанию
     *
     * @return список станций
     */
    public List<StationDto> load() {
        return load(fileNameDefault);
    }

    /**
     * Загрузить данные из файла
     *
     * @param fileName имя файла с полюсами
     * @return список станций
     */
    public List<StationDto> load(String fileName) {
        List<StationDto> result = new ArrayList<>();
        try (FileInputStream fstream = new FileInputStream(fileName)) {
            String strLine;
            BufferedReader br = new BufferedReader(new InputStreamReader(fstream));
            boolean isHeader = true;
            int simv = 0;
            while ((strLine = br.readLine()) != null) {
                if (!isHeader) {
                    String[] str = strLine.split("\\s");
                    StationDto station = new StationDto();
                    station.code = str[0];
                    //кол-во символов в строке до города                    
                    simv = str[1].length() + str[2].length() + 6;
                    String sub = strLine.substring(simv);
                    station.fullName = sub;
                    station.geoLon = Double.parseDouble(str[1]);
                    if (station.geoLon < 0 || station.geoLon > 360) {
                        logger.info("geoLon out of permission range");
                        while (station.geoLon < 0) {
                            station.geoLon += 360;
                        }
                        while (station.geoLon > 360) {
                            station.geoLon -= 360;
                        }
                    }
                    station.geoLat = Double.parseDouble(str[2]);
                    if (station.geoLat < -90 || station.geoLat > 90) {
                        logger.error("geoLat out of range");
                        break;
                    }
                    result.add(station);
                    logger.debug(station.code + " station code readed");
                } else {
                    isHeader = false;
                }
            }
        } catch (Exception ex) {
            String message = " error read stations " + fileName;
            logger.error(message);
            throw new IllegalStateException(ex);
        }
        return result;

    }
}
