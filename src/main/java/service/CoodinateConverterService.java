/*
 * ISTP, Irkutsk
 */
package service;

import dao.OutputGeoToDipDao;
import dao.PoleDao;
import dao.StationDao;
import static domain.CoordinateConverter.*;

import dto.PoleDto;
import dto.StationDto;
import dto.StationYearDto;

import static helper.AngleHelper.*;
import static java.lang.Math.PI;
import static java.lang.Math.toDegrees;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import org.slf4j.LoggerFactory;

/**
 * Преобразование координат: географическую в дипольную, дипольную в географическую
 *
 * @author Lev Broman
 */
public class CoodinateConverterService {

    private final org.slf4j.Logger logger = LoggerFactory.getLogger(this.getClass());

    /**
     * Выполнить преобразование координат
     */
    public void exec() {
        List<PoleDto> poleList = new PoleDao().load();
        List<StationDto> stationList = new StationDao().load();
        List<StationYearDto> list = calcListGeoToDip(poleList, stationList);
        List<StationYearDto> list2 = calcListDipToGeo(list);
              
        Collections.sort(list);
              
        new OutputGeoToDipDao().save(poleList, stationList);
        new OutputGeoToDipDao().saveDip("dip2.txt", list);
        new OutputGeoToDipDao().saveGeo("geo.txt", list2, stationList);
    }

    /**
     * Выполнить преобразование из ГСК в ДСК
     *
     * @param poleList список полюсов
     * @param stationList список станций
     * @return список с координатами станций в ДСК в различные эпохи
     */
    private List<StationYearDto> calcListGeoToDip(List<PoleDto> poleList, List<StationDto> stationList) {
        List<StationYearDto> result = new ArrayList<>();
        poleList.forEach((pole) -> { //статическая операция
            stationList.stream().map((station) -> { //функциональная операция 
                double poleTheta = latToTheta(pole.dipNorthLat);
                double poleLambda = lonToLambda(pole.dipNorthLon);
                double theta = latToTheta(station.geoLat);
                double lambda = lonToLambda(station.geoLon);
                double dipTheta = calcDipTheta(poleTheta, poleLambda, theta, lambda);
                double dipLambda = calcnewDipLambda(poleTheta, poleLambda, theta, lambda, dipTheta);
                double psi = calcPsi(poleTheta, poleLambda, theta, lambda, dipTheta);

                StationYearDto dto = new StationYearDto();
                dto.dipLat = thetaToLat(dipTheta);
                dto.dipLon = lambdaToLon(dipLambda);
                dto.declination = toDegrees(psi);
                dto.stationDto = station;
                return dto;
            }).map((dto) -> {
                dto.poleDto = pole;
                return dto;
            }).forEachOrdered((dto) -> {
                result.add(dto);
            });
        });
        return result;
    }

    /**
     * Выполнить преобразование из ДСК в ГСК
     *
     * @param list список с координатами станций в ДСК в различные эпохи
     * @return список с координатами станций в ДСК в различные эпохи (географические координаты пересчитываются)
     */
    private List<StationYearDto> calcListDipToGeo(List<StationYearDto> list) {
        List<StationYearDto> result = this.copy(list);
        for (int i = 0; i < list.size(); i++) {
            StationYearDto dto = list.get(i);
            double poleLambda = lonToLambda(dto.poleDto.dipNorthLon);
            double dipTheta = latToTheta(dto.dipLat);
            double geoTheta = latToTheta(dto.stationDto.geoLat);
            double poleTheta = latToTheta(dto.poleDto.dipNorthLat);
            double dipLambda = lonToLambda(dto.dipLon);
            double geoLambda = calcnewGeoLambda(poleLambda, poleTheta, dipTheta, dipLambda, geoTheta);
            double geoTheta2 = calcnewGeoTheta(dipTheta, dipLambda, poleTheta, poleLambda, geoLambda);
            StationYearDto dto2 = result.get(i);
            dto2.stationDto.geoLat = thetaToLat(geoTheta2);
            dto2.stationDto.geoLon = lambdaToLon(geoLambda);
        }
        return result;
    }

    /**
     * Сделать копию списка
     *
     * @param list список
     * @return копия списка
     */
    private List<StationYearDto> copy(List<StationYearDto> list) {
        List<StationYearDto> result = new ArrayList<>();
        for (StationYearDto dto : list) {
            StationYearDto dto2 = new StationYearDto(dto);
            result.add(dto2);
        }
        return result;
    }

   

}
