/*
 * ISTP, Irkutsk
 */
package service;

import dao.BorderDao;
import domain.MagneticFlux;
import static java.lang.Math.toRadians;
import static domain.MagneticFlux.magnetFlux;
import dto.BorderDto;
import java.util.List;
import static helper.AngleHelper.colatToThetaArray;

/**
 *
 * @author Lev Broman
 */
public class PolarCapService {

    public void polarCapFlux() {
        double[] border = getFakeBorder();
        double flux = magnetFlux(border);
        System.out.println("На кошироте 15 градусов flux= " + flux + " вебер");
    }

    public void MagnetFluxFile() {
        List<BorderDto> borderList = new BorderDao().load();
        int n = borderList.size();
        double[] border = new double[n];
        for (int i = 0; i < n; i++) {
            border[i] = borderList.get(i).colat;
        }
        border = colatToThetaArray(border);
        double flux = MagneticFlux.magnetFluxInGW(border);
        System.out.println("Flux Experiment=" + flux + " Giga Weber ");
    }

    /**
     * Получить границу из 36 долгот на кошироте 15 градусов, рад
     *
     * @return граница
     */
    private double[] getFakeBorder() {
        int dLon = 10;
        int n = 360 / dLon;
        double[] border = new double[n];
        for (int i = 0; i < n; i++) {
            border[i] = toRadians(15);
        }
        return border;
    }

}
