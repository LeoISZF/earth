/*
 * ISTP, Irkutsk
 */
package domain;

import static domain.Constants.*;
import static helper.ValueConverterHelper.toGiga;
import static java.lang.Math.PI;
import static java.lang.Math.pow;
import static java.lang.Math.sin;
import static java.lang.Math.toRadians;

/**
 * Лунюшкин С.Б. Магнитный поток через полярную шапку.pdf
 * 
 * вебер=Вб, гигавебер=ГВб
 *
 * @author Lev Broman
 * 
 * 
 */
public class MagneticFlux {

    /**
     * Поток через полярную шапку
     *
     * Формула 7
     *
     * @param border граница массив коширот в радианах
     * @return магнитный поток через заданную границу, вебер
     *
     */
    public static double magnetFlux(double[] border) {
        //проверка на радианы
        double dLon = border.length;
        double flux = 0;
        //  перевод в лямбда проверка на выход за границы 
        double dLambda = toRadians(dLon);
        double C = mu0 * MAGNETIC_MOMENT / (4 * PI * (EARTH_RADIUS + HEIGHT_OF_IONOSPHERE)) * dLambda;
        double nLon = 360 / dLon;
        for (int iLon = 0; iLon < nLon; iLon++) {
            double theta = border[iLon];
            flux += pow(sin(theta), 2);
        }
        return C * flux;
    }

    /**
     * Поток через полярную шапку
     *
     * Формула 7
     *
     * @param border граница массив коширот в радианах
     * @return магнитный поток через заданную границу, ГВб
     *
     */
    public static double magnetFluxInGW(double[] border) {
        return toGiga(magnetFlux(border));
    }



}
