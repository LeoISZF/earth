/*
 * ISTP, Irkutsk
 */
package domain;

import static helper.AngleHelper.*;
import helper.*;
import static helper.MathHelper.safeAcos;
import static java.lang.Math.*;
import static java.lang.StrictMath.abs;

/**
 * Преобразование координат
 *
 * @author Lev Broman
 */
public class CoordinateConverter {

    public static double eps = 0.0001;

    /**
     * Вычислить дипольную кошироту, рад
     *
     * Коширота = [0..pi], долгота = [0..2pi]
     *
     * @param poleTheta коширота полюса, рад
     * @param poleLambda долгота полюса, рад
     * @param theta коширота точки, рад
     * @param lambda долгота точки, рад
     * @return дипольная коширота, рад
     */
    public static double calcDipTheta(double poleTheta, double poleLambda, double theta, double lambda) {
        double dipTheta;
        double arg = lambda - poleLambda;
        if (arg < 0) {
            arg = -arg;
        } else if (lambda - poleLambda == 0) {
            dipTheta = abs(theta - poleTheta);
            check(isValidTheta(dipTheta));
            dipTheta = isValue2pi(dipTheta);
            return dipTheta;
        } else {
        }
        dipTheta = safeAcos(cos(theta) * cos(poleTheta) + sin(theta) * sin(poleTheta) * cos(arg));

        if (abs(poleTheta) < eps) {
            dipTheta = theta;
        }
        if (abs(poleTheta - PI) < eps) {
            dipTheta = PI - theta;
        }

        check(isValidTheta(dipTheta));
        dipTheta = isValue2pi(dipTheta);
        return dipTheta;
    }

    /**
     *
     * Вычислить магнитное склонение, рад
     */
    public static double calcPsi(double poleTheta, double poleLambda, double geoTheta, double geoLambda, double dipTheta) {
        double psi;
        if (abs(sin(geoTheta)) > eps && abs(sin(dipTheta)) > eps) {
            double a = safeAcos((cos(poleTheta) - cos(dipTheta) * cos(geoTheta)) / (sin(dipTheta) * sin(geoTheta)));
            boolean expression = TriangleNewOrientation(geoLambda, poleLambda);//true when psi<0
            psi = a;
            if (expression) {//psi<0

                if (psi > 0) {
                    psi = -psi;
                }

            } else {//psi>0
                if (psi < 0) {
                    psi = -psi;
                }
            }
        } else {
            psi = 0;
        }
        if (abs(poleTheta) < eps) {// гео=дип полюсы
            psi = 0;
        }
        if (abs(geoTheta - poleTheta) < eps && abs(geoLambda - poleLambda) < eps) {//станция в дип юж полюсе 
            psi = 0;
        }
        return psi;

    }

    /**
     * @deprecated Вычислить дипольную долготу, рад все параметры принимаются на вход в радианах
     */
    public static double calcDipLambda(double poleTheta, double dipTheta, double theta, double lambda, double poleLambda) {
        double dipLambda = 0;
        if (abs(sin(poleTheta)) > eps && abs(sin(dipTheta)) > eps) {
            if (sin(dipTheta) == 0 || sin(dipTheta) == PI) {
                System.out.println("error calcdiplambda(DipTH)");
            }
            double a = safeAcos((cos(theta) - cos(dipTheta) * cos(poleTheta)) / (sin(dipTheta) * sin(poleTheta)));
            if (lambda < (poleLambda + 2 * PI) && lambda > (poleLambda + PI)) {
                dipLambda = PI + a;
            } else {
                dipLambda = PI - a;
            }
        } else {
            //дип юж полюс в одном из географических
            if (abs(sin(poleTheta)) < eps) {
                //if (abs(cos(poleTheta)-1) < eps) {
                //нет магнитного меридиана 
                dipLambda = lambda;
            }
            //станция в одном из дип полюсов
            if (abs(sin(dipTheta)) < eps) {
                //в южн dip
                dipLambda = 0;
                //в сев dip
                dipLambda = 0;//pi or 0
            }
        }
        if (theta < eps) {
            dipLambda = PI;
        }
        return dipLambda;
    }

    /**
     *
     * @deprecated
     */
    public static double calcGeoTheta(double dipTheta, double dipLambda, double poleTheta, double lambda, double poleLambda) {
        double theta;
        if (sin(dipLambda) >= 0) {
            theta = safeAcos(cos(dipTheta) * cos(poleTheta) + sin(dipTheta) * sin(poleTheta) * cos(PI - dipLambda));
        } else {
            theta = safeAcos(cos(dipTheta) * cos(poleTheta) + sin(dipTheta) * sin(poleTheta) * cos(dipLambda - PI));
        }
        return theta;
    }

    /**
     * Вычислить географическую долготу станцию по данным в дипольной системе координат
     *
     * @param poleLambda
     * @param dipTheta
     * @param geoTheta
     * @param poleTheta
     * @param dipLambda
     * @return
     */
    public static double calcnewGeoLambda(double poleLambda, double poleTheta, double dipTheta, double dipLambda, double geoTheta) {
        double lambda = 0;
        if (abs(sin(geoTheta)) > eps && abs(sin(poleTheta)) > eps) {
            double a = safeAcos((cos(dipTheta) - cos(geoTheta) * cos(poleTheta)) / (sin(geoTheta) * sin(poleTheta)));
            double arg = (cos(dipTheta) - cos(geoTheta) * cos(poleTheta)) / (sin(geoTheta) * sin(poleTheta));
            if (PI - dipLambda > 0) {
                lambda = poleLambda + a;

            } else {
                lambda = poleLambda - a;
            }
        } else {
            //станция в одном из гео полюсов
            if (abs(sin(geoTheta)) < eps) {
                lambda = 0;//                
            }
            //дип юж полюс в сев или южн гео полюсе
            if (abs(sin(poleTheta)) < eps) {
                lambda = dipLambda;
                return lambda;
            }
        }

        if (abs(sin(dipTheta)) < eps) {//станция в дип полюсе    

            if (abs(dipTheta) < eps) {
                lambda = poleLambda;
            } else {
                if (poleLambda > PI) {
                    lambda = poleLambda - PI;
                } else {
                    lambda = poleLambda + PI;
                }
            }
        }
        lambda = isOutOfCircle(lambda);

        check(isValidLambda(lambda));
        lambda = isValue2pi(lambda);
        return lambda;
    }


    public static double calcnewDipLambda(double poleTheta, double poleLambda, double theta, double lambda, double dipTheta) {
        double dipLambda = 0;
        if (abs(sin(poleTheta)) > eps && abs(sin(dipTheta)) > eps) {
            double a = safeAcos((cos(theta) - cos(dipTheta) * cos(poleTheta)) / (sin(dipTheta) * sin(poleTheta)));

            boolean expression = TriangleNewOrientation(lambda, poleLambda);
            if (expression) {
                dipLambda = PI - a;
            } else {

                dipLambda = PI + a;
            }
        } else {
            //дип юж полюс в одном из географических
            if (abs(sin(poleTheta)) < eps) {
                //if (abs(cos(poleTheta)-1) < eps) {
                //нет магнитного меридиана 
                dipLambda = lambda;
            }
            //станция в одном из дип полюсов
            if (abs(sin(dipTheta)) < eps) {
                //в южн dip
                dipLambda = 0;
                //в сев dip
                dipLambda = 0;//pi or 0
            }
        }
        if (abs(theta) < eps) {//станция на сев гео полюсе 
            dipLambda = PI;
        }
        check(isValidLambda(dipLambda));
        dipLambda = isValue2pi(dipLambda);
        return dipLambda;
    }

    /**
     * @deprecated
     *
     */
    public static double calcGeoLambda(double poleLambda, double dipTheta, double geoTheta, double poleTheta, double dipLambda) {
        double lambda;
        // на вход в файле принимаем долготу (0-360) от 0 до 360 градусов

        if (abs(sin(geoTheta)) > eps && abs(sin(poleTheta)) > eps) {
            double a = safeAcos((cos(dipTheta) - cos(geoTheta) * cos(poleTheta)) / (sin(geoTheta) * sin(poleTheta)));
            if (sin(dipLambda) >= 0) {
                lambda = poleLambda + a;
            } else {
                lambda = poleLambda - a;
            }
        } else {
            lambda = 0;
        }
        if (abs(sin(dipTheta)) < eps) {
            lambda = poleLambda;
        }
        if (lambda < 0) {
            lambda = 2 * PI + lambda;
        }
        return lambda;
    }

    public static double calcnewGeoTheta(double dipTheta, double dipLambda, double poleTheta, double poleLambda, double lambda) {
        double theta;
        if ((lambda - dipLambda) > 0) {
            theta = safeAcos(cos(dipTheta) * cos(poleTheta) + sin(dipTheta) * sin(poleTheta) * cos(PI - dipLambda));
        } else {
            theta = safeAcos(cos(dipTheta) * cos(poleTheta) + sin(dipTheta) * sin(poleTheta) * cos(dipLambda - PI));
        }
        theta = isOutOfCircle(theta);
        check(isValidTheta(theta));
        return theta;
    }

}
