/*
 * ISTP, Irkutsk
 */
package domain;

import static java.lang.Math.PI;
import static java.lang.Math.pow;

/**
 *
 * @author Lev Broman
 */
class Constants {
   final static double  mu0=4*PI*pow(10,-7);// Гентри/метр
   /**
    * Среднпий радиус Земли
    */
   final static double EARTH_RADIUS=6371220;// метры
   /**
    * Мгнитный момент Земли
    */
   final static double MAGNETIC_MOMENT=7.79*pow(10,22);//Ампер*(метр в квадрате)
   /**
    * Высота Расчета магнитного потка яерезз полярную шапку
    * Высота на которой течет ток
    */
   final static double HEIGHT_OF_IONOSPHERE=115000;//метры 
   
}
