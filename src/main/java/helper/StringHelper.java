/*
 * ISTP, Irkutsk
 */
package helper;

/**
 *
 * @author Lev Broman
 */
public class StringHelper {

    public static String repeat(String s, int n) {
        String result = "";
        for (int i = 0; i < n; i++) {
            result += s;
        }
        return result;
    }
}
