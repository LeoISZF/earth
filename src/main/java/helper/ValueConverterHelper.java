/*
 * ISTP, Irkutsk
 */
package helper;

/**
 *
 * @author Lev Broman
 */
public class ValueConverterHelper {

    public static final double MEGA = 1E6;
    public static final double GIGA = 1E9;

    /**
     * Преобразовать значение в гигазначение
     *
     * @param x значение
     * @return гигазначение
     */
    public static double toGiga(double x) {
        return x / GIGA;
    }

}
