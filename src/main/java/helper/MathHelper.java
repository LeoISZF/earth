/*
 * ISTP, Irkutsk
 */
package helper;

import static java.lang.Math.abs;
import static java.lang.Math.acos;

/**
 *
 * @author Lev Broman
 */
public class MathHelper {
/**
 * Безопасный арккосинус
 * 
     * @param arg
 * @return 
 */
    public static double safeAcos(double arg) {
        if (arg > 1) {
            if (abs(arg-1) < 0.001) {
                arg = 1;
            }
        }
        if (arg < -1) {
            if (abs(arg + 1) < 0.001) {
                arg = -1;
            }
        }
        return acos(arg);
    }
    
    public static double close_to_zero(double arg){
        if(abs(arg-0)<0.00001){
            return 0;
        }
        else return arg;
        
    }
    
    //метод сравнения 
    

}
