/*
 * ISTP, Irkutsk
 */
package helper;

import static helper.ArrayHelper.op;
import static helper.MathHelper.close_to_zero;

import static java.lang.Math.*;
import static java.lang.StrictMath.abs;

/**
 * Функции преобразования углов
 *
 * @author Lev Broman
 */
public class AngleHelper {

    public static double eps = 0.0001;

    /**
     * Преобразовать географическую кошироту станции (рад) в широту (градус)
     *
     * @param theta географическая широта
     * @return
     */
    public static double thetaToLat(double theta) {
        check(isValidTheta(theta));
        return colatToLat(toDegrees(theta));
    }

    public static double latToTheta(double lat) {
        return toRadians(latToColat(lat));
    }

    public static double latToColat(double lat) {
        double colat = 90 - lat;
        return colat;
    }

    public static double colatToLat(double colat) {
        check(isValidColat(colat));
        double lat = 90 - colat;
        return lat;
    }

    public static double colatToTheta(double colat) {
        check(isValidColat(colat));
        return toRadians(colat);
    }
    //

    //проверка на выход за границы диапазона
    public static double isOutOfCircle(double val) {
        while (val >= 2 * PI) {
            val -= 2 * PI;
        }
        while (val < 0) {
            val += 2 * PI;
        }
        return val;
    }

    //проверка на равенство 2pi
    public static double isValue2pi(double v) {
        if (abs(v - 2 * PI) < eps) {
            return 0;
        } else {
            return v;
        }
    }

    /**
     * Определить ориентацию сферического треугольника, что правее станция или дипольный полюс
     *@deprecated 
     * @param lambda
     * @param poleLambda
     * @return true when station more to the right than the south dipole magnetic pole also : psi<0 ; (dipLambda-pi)<0
     * 
     */
    public static boolean OrientTriangle(double lambda, double poleLambda) {
        lambda = isOutOfCircle(lambda);
        poleLambda = isOutOfCircle(poleLambda);
        if ((lambda > PI && poleLambda > PI) || (lambda < PI && poleLambda < PI)) {
            if (lambda - poleLambda > 0) {
                return true;
            } else {
                return false;
            }
        }
        if (abs(lambda) < eps && PI < poleLambda && poleLambda < 2 * PI) {
            return true;
        } else if (abs(lambda) < eps && poleLambda < PI) {
            return false;
        }
        if (abs(lambda - PI) < eps && poleLambda > 0 && poleLambda < PI) {
            return true;
        } else if (abs(lambda - PI) < eps && poleLambda > PI) {
            return false;
        }
        if (abs(poleLambda - PI) < eps && lambda > PI && lambda < 2 * PI) {
            return true;
        } else if (abs(poleLambda - PI) < eps && lambda < PI) {
            return false;
        }
        if (poleLambda > PI && lambda < PI) {
            return true;
        } else if (poleLambda < PI && lambda > PI) {
            return false;
        }
        if (abs(poleLambda) < eps && lambda > PI) {
            return false;
        }

        System.out.println("b");
        boolean b = false;
        return b;
    }
    /**
     * Определить ориентацию сферического треугольника
     * @param lambda
     * @param poleLambda
     * @return true when psi<0 (или станция правее дип южного полюса)
     */
    public static boolean TriangleNewOrientation(double lambda, double poleLambda) {
        lambda = isOutOfCircle(lambda);
        poleLambda = isOutOfCircle(poleLambda);
        boolean bul = false;
        if (poleLambda<PI){
            if (lambda>poleLambda&&(lambda-poleLambda)<PI) {
                bul=true;//psi<0
            }else if(lambda<poleLambda||(lambda>poleLambda&&(lambda-poleLambda)>PI)){
                bul=false;
            }            
        }else if(poleLambda>PI){
            if (lambda>poleLambda||(lambda<poleLambda&&(poleLambda-lambda)>PI)) {
                bul=true;                
            } else if(lambda<poleLambda&&(poleLambda-lambda)<PI){
                bul=false;
                
            }            
        }
        if (abs(lambda)<eps) {
            if(poleLambda>PI){
                bul=true;
            }
            if (poleLambda<PI) {
              bul=false;
            }            
        }
        if(abs(lambda-PI)<eps){
            if(lambda>poleLambda){
                bul=true;
            }
            if(lambda<poleLambda){
                bul=false;
            }
        }
        if(abs(poleLambda)<eps){
            if(lambda<PI){
                bul=true;
            }
            if(lambda>PI){
                bul=false;
            }
        }
        if(abs(poleLambda-PI)<eps){
            if(lambda>PI){
               bul=true;
            }
            if(lambda<PI){
                bul=false;
            }
        }

        
        
        return bul;
    }

    public static boolean isValidColat(double colat) {
        return (colat >= 0) && (colat <= 180);
    }

    public static boolean isValidTheta(double theta) {
        return (theta >= 0) && (theta <= PI);
    }

    public static boolean isValidLambda(double lambda) {
        if ((lambda < 0) && (lambda > 2 * PI)) {
            System.out.println("lambda error  = " + lambda);
        }
        return (lambda >= 0) && (lambda <= 2 * PI);
    }

    public static void check(boolean x) {
        if (!x) {
            throw new IllegalStateException("args " + x);
        }
    }

    /**
     * Преобразовать долготу станции (градус) в долготу (радианы)
     *
     * @param lon географическая долгота
     * @return
     */
    public static double lonToLambda(double lon) {
        double lambda = toRadians(lon);
        lambda = close_to_zero(lambda);

        while (lambda > 2 * PI) {
            lambda -= 2 * PI;
        }
        while (lambda < 0) {
            lambda += 2 * PI;
        }
        return lambda;
    }

    public static double lambdaToLon(double lambda) {
        return toDegrees(lambda);
    }

    /**
     * Масиив широт в массив коширот
     *
     * @param massLat
     * @return масиив коширот
     */
    public static double[] colatToThetaArray(double[] massLat) {
        return ArrayHelper.op(massLat, AngleHelper::colatToTheta);

    }

//    private static double toDegrees(double theta) {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
//    }
}
