/*
 * ISTP, Irkutsk
 */
package helper;

import java.util.function.DoubleUnaryOperator;

/**
 *
 * @author Lev Broman
 */
public class ArrayHelper {
/**
 * Применить операцию к массиву 
 * @param x массив
 * @param op унарная функция(от одного агумента)
 * @return 
 */
static double[] op(double[] x, DoubleUnaryOperator op) {
        double[] result = new double[x.length];
        for (int i = 0; i < x.length; i++) {
            result[i] = op.applyAsDouble(x[i]);
        }
        return result;
    }

    public static double[] colatToTheta(double[] x) {
        return op(x, AngleHelper::colatToTheta);
    }

}
