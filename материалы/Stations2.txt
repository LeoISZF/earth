IAGA	GLON	GLAT	STATION-NAME
SPA	0	-90	South Pole Station
B23	316,13	-88,03	m88-316
B21	28,41	-87	m87-028
B22	68,17	-86,51	m87-069
B19	2,06	-85,36	m85-002
B20	95,98	-85,36	m85-096
PG1	77,2	-85,5	Antartica
B18	336,14	-84,35	m84-336
PG2	57,96	-84,42	Antartica
PG3	37,62	-84,42	Antartica
B17	347,76	-82,9	m83-348
B16	347,06	-82,78	m83-347
PG4	12,2	-83,34	Antartica
B14	337,74	-80,89	m81-338
B15	2,97	-81,49	m82-003
B27	3	-81	m81-003
B12	335,88	-79,08	m79-336
B13	77	-80	m80-077
SBA	166,78	-77,85	Scott Base
VOS	106,87	-78,45	Vostok
MCM	166,67	-77,85	McMurdo Station
B11	336,58	-77,51	m78-337
B10	39,71	-77,32	m77-040
DMC	124,17	-75,25	Dome Concordia
B09	42,99	-74,01	m74-043
B08	159,03	-72,77	m73-159
B07	44,28	-70,7	m70-044
B24	77,77	-71,67	m72-078
NVL	11,83	-70,78	Novolazorevskaya
VNA	351,7	-70,7	Neumayer Station III
DVS	77,97	-68,58	Davis
B04	41,08	-68,58	m68-041
B05	41,32	-69,29	m69-041
B06	39,4	-69,67	m70-039
PRG	76,23	-69,23	Progress
DRV	140,01	-66,67	Dumont Durville
MAW	62,88	-67,61	Mawson
B03	291,88	-67,57	m67-292
CSY	110,53	-66,28	Casey
MIR	93,02	-66,55	Mirny
AIA	295,74	-65,25	Faraday Islands
B02	294	-66,03	m66-294
PAL	295,95	-64,77	Palmer
B01	296,52	-64,82	m65-297
LIV	299,61	-62,66	Livingston Island
OHI	302,1	-63,32	OHiggins
ESC	301,08	-62,18	Escudero
ORC	315,26	-60,74	Orcadas
KEP	323,51	-54,28	King Edward Point
MCQ	158,95	-54,5	Macquarie Island
PNT	289,1	-53,2	Puerto Natales
PST	302,11	-51,7	Port Stanley
ENP	289,1	-52,13	San Gregorio
PAF	70,26	-49,35	Port aux Francias
CZT	51,87	-46,43	Port Alfred
EYR	172,4	-43,4	Eyrewell
TRW	294,68	-43,25	Trelew
LEM	147,5	-42,3	Lemont
HOB	147,35	-42,88	Hobart
VLD	286,86	-39,48	Valdivia
OSO	286,91	-40,34	Osorno
PAC	289,91	-40,34	Punta Arenas
MLB	145,18	-38,36	Melbourne
TDC	347,69	-37,1	Tristan de Cunha
AMS	77,57	-37,8	Martin de Vivias
CAN	149,36	-35,3	Canberra
CNB	150,7	-34,1	Canberra
HER	19,23	-34,43	Hermanus
KAT	117,62	-33,68	Katanning
ADL	138,65	-34,67	Adelaide
CER	289,4	-33,45	Los Cerrillos
CMD	150,67	-34,06	Camden
GNA	116	-31,8	Gnangara
SUT	20,67	-32,4	Sutherland
PIL	294,47	-31,4	Pilar
GNG	115,71	-31,36	Gingin
SER	288,87	-30	La Serena
CUL	149,58	-30,28	Culgoora
DRB	30,56	-29,48	Durban
G01	306,28	-29,72	Santa Maria
IPM	250,58	-27,2	Isla de Pascua
KMH	18,11	-26,54	Keetmanshoop
DAL	151,2	-27,18	Dalby
HBK	27,71	-25,88	Hartebeesthoek
BSV	139,21	-25,54	Birdsville
LMM	40	-25,92	Maputo
ASP	133,88	-23,77	Alice Springs
VSS	316,35	-22,4	Vassouras
ANT	289,76	-23,39	Antofagasta
LRM	114,1	-22,22	Learmonth
ROC	150,31	-23,19	Rockhampton
CTA	146,3	-20,1	Charters Towers
TAN	47,55	-18,92	Antananarivo
TSU	17,7	-19,22	Tsumeb
PUT	290,5	-18,33	Putre
A05	17,58	-19,2	Namibia
TWN	146,82	-19,27	Townsville
VRE	292,38	-17,28	Villa Remedios
PPT	210,42	-17,57	Pamatai
SHE	354,27	-15,95	St, Helena
NMP	39,25	-15,09	Nampula
CKT	145,25	-15,48	Cooktown
LSK	28,19	-15,23	Lusaka
API	188,22	-13,8	Apia
ASA	170,7	-14,28	American Samoa
ICA	-75,75	-14,08	Ica
DRW	130,9	-12,4	Darwin
WEP	141,88	-12,68	Weipa
KDU	132,47	-12,69	Kakadu
HUA	284,67	-12,05	Huancayo
CKI	96,84	-12,19	Cocos-Keeling Islands
ANC	282,85	-11,77	Ancon
A10	319,5	-9,4	Petrolina
KPG	123,4	-10,2	Kupang
WTK	112,63	-7,56	Watukosek
ASC	345,62	-7,95	Ascension Island
DES	39,12	-6,46	Dal Es Salaam
LWA	104,06	-5,02	Liwa
WEW	143,62	-3,55	Wewak
BKL	102,31	-3,86	Bengkulu
EUS	-38,43	-3,88	Eusebio
PRP	119,4	-3,6	ParePare
BIK	136,05	-1,08	Biak
A11	311,55	-1,45	Belem
PTN	109,25	-0,05	Pontianak
KTB	100,32	-0,2	Kototabang
NAB	36,48	-1,16	Nairobi
TND	124,95	1,29	Tondano
GAN	73,15	0,69	Gan
JRS	-78,3	0	Alem Ecua
SCN	100,3	-0,55	Sicincin
GSI	97,34	1,18	Gunung Sitoli
MND	124,84	1,44	Manado Indon
JYP	140,7	2,51	Jayapura
BNG	18,57	4,33	Bangui
A03	11,52	3,87	Cameroon
KOU	307,27	5,21	Kourou
A06	355,87	5,33	Abidjan
KOR	134,5	7,33	Koror
A08	125,4	7	Davao
LAG	3,27	6,48	Lagos
LKW	99,78	6,3	Langkawi
SBH	116,07	6,02	Jayapura
A13	98,35	7,89	Phuket
AAE	38,77	9,03	Addis Ababa
YAP	138,5	9,3	Yap
A01	7,39	8,99	Abuja
CRP	275,09	10,44	Chiripa
A07	346,34	9,58	Conakry
BCL	105,71	9,3	Bac_Lieu
CDO	124,63	8,4	CagayanDeOro
ILR	4,68	8,5	Ilorin
TIR	76,95	8,48	Tirunelveli
CEB	123,91	10,36	Cebu
DLT	108,48	11,94	Dalat
GUA	144,87	13,59	Guam
LGZ	123,74	13,1	Legazpi
MBO	343,03	14,38	Mbour
MUT	121,02	14,37	Muntinlupa
A04	39,46	14,28	Ethiopia
A09	120	14,58	Manila
A12	100,61	14,08	Bangkok
PNL	303,82	16,68	Pantanal
HYB	78,6	17,4	Hyderabad
ABG	72,87	18,62	Alibag
SJG	293,85	18,11	San Juan
TGG	121,76	17,66	Tuguegarao
TEO	260,82	19,75	Teoloyucan
HON	202	21,32	Honolulu
PHU	105,95	21,03	Phuthuy
EWA	202	21,32	Ewa Beach
M11	256,9	20,7	Juriquilla
TAM	5,53	22,79	Tamanrasset
GZH	113,34	23,09	Guangzhou
ASW	32,51	23,5	Aswan
SON	66,44	25,12	Sonmiani
LNP	121,17	25	Lunping
M10	260,4	24,8	Linares
HLN	121,55	23,9	Hualien
CBI	142,3	27,15	Chichijima
M09	262,2	26,4	Lyford
JAI	75,8	26,92	Jaipur
GUI	343,57	28,32	Guimar
MID	182,62	28,21	Midway
FIT	279,05	28,07	Bullcreek
AMA	129,33	28,17	Amami-Oshima
FYM	35,5	29,18	Fayum
CDP	256,3	31	Chengdu
BSL	270,37	30,35	Bay St Louis
DLR	259,08	29,49	Del Rio
JAX	278,4	30,35	Jacksonville
ELT	34,95	29,67	Eilat
MLT	30,89	29,52	Misallat
M08	261,39	29,44	San Antonio
TUC	249,27	32,17	Tucson
KAG	130,72	31,48	Kagoshima
YMK	130,62	31,19	Yamakawa
KNY	130,88	31,42	Kanoya
BGY	35,09	31,72	Bar Gyora
HTY	139,8	33,12	Hatizyo
QSB	35,64	33,87	Qsaybeh
USC	278,54	33,34	U of South Carolina
M07	262,25	32,98	Richardson
M06	262,6	35	Purcell
T26	242,15	34,2	San Gabriel
KRT	13,02	33,37	Khartoum
KUJ	131,23	33,06	Kuju
T27	242,32	34,38	Table Mountain
LZH	103,85	36,09	Lanzhou
FRN	240,3	37,1	Fresno
SMA	334,87	36,99	Santa Maria/Azoren
KAK	140,18	36,23	Kakioka
TUL	264,22	35,92	Tulsa
DSO	278,6	36,25	Darsky
SFS	354,06	36,67	San Fernando
E05	22,95	36,72	Velies
CYG	126,85	36,37	Cheongyang
A02	2,93	36,85	Algeria
FRD	282,63	38,2	Fredericksburg
ASH	58,1	37,95	Ashkabad
ONW	141,47	38,43	Onagawa
PEG	23,9	38,1	Pedeli
BOU	254,77	40,13	Boulder
APL	283,12	39,17	Applied Physics Lab
ESA	141,36	39,24	Esashi
MIZ	141,2	39,11	Mizusawa
M05	263,7	38,5	Americus
T16	240,2	39,19	Carson City
BMT	116,2	40,3	Beijing Ming Tombs
SPT	355,65	39,55	San Pablo Toledo
TOL	355,95	39,88	Toledo
BJI	116,18	40,06	Beijing
E02	21,97	39,45	Klokotos
E03	24,1	38,63	Kimi
E04	23,93	38,08	Dionyssos
ISK	29,06	41,07	Kandilli
TKT	69,62	41,33	Tashkent
EBR	0,49	40,82	Ebro
T20	281,62	40,18	Loysburg
M04	263,84	41,36	Bennington
IZN	29,73	40,5	Iznik
E01	23,86	41,35	Nevrokopi
AQU	13,32	42,38	LAquila
DUR	14,28	41,39	Duronia
C01	276,1	42,42	Ann Arbor
MMB	144,19	43,91	Memambetsu
AAA	76,92	43,25	Alma Ata
PPI	131,73	42,98	Popov Island
RIK	143,76	43,48	Rikubetsu
MSH	288,52	42,6	Millstone Hill
GTF	288,05	43,62	Grafton
M03	264,4	43,6	Worthington
OTT	284,45	45,4	Ottawa
LON	16,66	45,41	Lonjsko Polje
SUA	26,25	45,32	Surlari
MSR	142,27	44,37	Moshiri
CLK	285	44,7	Clarkson
GCK	20,77	44,63	Grocka
SBL	299,99	43,93	Sable Carrigan
T21	257,41	43,08	Pine Ridge
T23	274,86	43,66	Remus
CNH	124,86	44,08	Changchun
WMQ	87,71	43,81	Urumqi
RNC	12,08	43,97	Ranchio
ASB	142,17	43,46	Ashibetu
NKK	62,12	45,77	Novokazalinsk
ODE	30,88	46,22	Odessa
M02	266,75	45,56	Cambridge
STJ	307,32	47,6	St Johns
THY	17,54	46,9	Tihany
M01	263,55	46,87	Glyndon
C08	264,92	45,87	Osakis
T17	287,87	44,95	Derby
T24	271,4	44,78	Shawano
T15	275,66	46,24	Bay Mills
T18	259,32	46,09	Fort Yates
T25	241,07	45,14	Ukiah
CST	11,65	46,05	Castello Tesino
P01	16,66	45,41	Lonjsko Polje
VIC	236,58	48,52	Victoria
NEW	242,88	48,27	Newport
CLF	2,27	48,02	Chambon la foret
FUR	11,28	48,17	Furstenfeldbruk
HRB	18,19	47,86	Hurbanovo
NCK	16,72	47,63	Nagycenk
YSS	142,72	46,95	Yuzhno Sakhalinsk
BDV	14,02	49,07	Budkov
VLO	282,24	48,19	Val-dOr
BFO	8,33	48,33	Black Forest
C10	245,79	47,66	Polson
C11	263,64	48,03	Thief River Falls
T19	245,33	47,61	Hot Springs
PAG	24,18	47,49	Panagjurishte
KHB	134,69	47,61	Khabarovsk
VYH	18,84	48,49	Vyhne
WIC	15,52	47,55	Conrad Observatorium
PIN	263,96	50,2	Pinawa
MZH	117,4	49,6	Manzaoli
GLN	262,88	49,65	Glenlea
LET	247,13	49,64	Lethbridge
T50	287,55	48,65	Saint-Fellicien
T51	282,22	48,05	Val-dOr
HVD	91,67	48,01	Khovd
KIV	30,3	50,72	Kiev
KGD	73,08	49,82	Karaganda
LVV	23,75	49,9	Lvov
T30	285,6	49,8	Chibougamau
T32	277,7	49,4	Kapuskasing
BRD	260,03	49,87	Brandon
WHS	264,75	49,8	Whiteshell
VAL	349,75	51,93	Valentia
HAD	355,52	50,98	Hartland
MAB	5,68	50,3	Manhay
DOU	4,6	50,1	Dourbes
BEL	20,8	51,83	Belsk
ROT	245,87	51,07	Rothney
T03	247,02	50,37	Vulcan
C04	251,74	50,06	Gull Lake
C12	256,2	49,69	Weyburn
T43	245,7	50,9	Priddis
ZAG	20,58	50,28	Zagorzyce
T49	293,73	50,22	Spet-lles
NGK	12,68	52,07	Niemegk
IRT	104,45	52,17	Irkoutsk
RED	246,16	52,14	Red Deer
MSK	34,52	52,69	MSK
C13	239,97	51,88	Wells Gray
C05	264,54	52,03	Little Grand Rapids
WAD	356,1	51,9	Wadena
MEA	246,65	54,62	Meanook
WNG	9,07	53,75	Wingst
ISL	265,34	53,86	Island Lake
LAN	357,23	54,01	Lancaster
YOR	358,95	53,95	York
EDM	246,7	53,3	Edmonton
SAS	253,6	52,8	Saskatoon
C06	247,03	53,35	Ministik Lake
T28	299,5	53,3	Goose Bay
PET	158,25	52,97	Paratunka
SZC	19,61	52,91	Szczechowo
PBQ	282,26	55,28	Poste de la Baleine
ESK	356,8	55,32	Eskdalemuir
HLP	18,82	54,61	Hel
MNK	26,52	54,1	Minsk
BFE	11,67	55,62	Brorfelde
ROE	8,55	55,17	Roemoe
NVS	82,9	55,03	Novosibirsk
RSV	12,45	55,85	Rude Skov
MOS	37,32	55,48	Moscow
C09	264,71	54,93	Oxford House
T33	259,1	54	Flin Flon/The Pas
T36	246,69	54,71	Athabasca
T37	237,2	53,8	Prince George
SUW	23,18	54,01	Suwalki
T52	282,38	53,79	Radisson
PTK	158,25	52,94	St,Paratunka
T42	254,84	55,15	La Ronge
SIT	224,67	57,07	Sitka
GIM	265,36	56,38	Gillam
FMC	248,79	56,66	Fort McMurray
NAN	298,3	56,4	Nain
FSJ	239,27	56,23	Fort St, John
KNZ	48,85	55,83	Zaymishche
SHU	199,54	55,38	Shumagin
T45	282,24	55,28	Kuujjuarapik
T48	293,19	54,8	Schefferville
KLD	20,2	54,5	Kaliningrad
T31	280,8	56,5	Sanikiluaq
ARS	58,57	56,43	Arti
BRZ	24,75	56,21	Birzai
BOX	38,97	58,03	Barok
BOR	38,33	58,03	Borok
CRK	357,36	57,09	Crooktree
GML	356,32	57,16	Gleenmore Lodge
LOV	17,83	59,35	Lovo
FCC	265,92	58,76	Fort Churchill
RAL	256,32	58,22	Rabbit Lake
FVE	243,98	58,37	Fort Vermilion
C02	265,79	57,71	Back Lake
T22	226,84	56,83	Petersburg
T29	291,8	58,3	Kuujjuaq
T44	281,95	58,47	Inukjuak
LER	358,82	60,13	Lerwick
SMI	248,05	60,02	Fort Smith
KAR	5,24	59,21	Karmoey 
TAR	26,46	58,26	Tartu
LNN	30,7	59,95	Leningrad
YAK	129,72	60,02	Yakutsk
MGD	150,86	59,97	Magadan
KVI	17,63	59,5	Kvistaberg
HOM	209,53	59,7	Homer
C03	248,89	58,77	Fort Chipewyan
AMU	210,1	61,2	Anchorage
SPG	29,72	60,54	Saint Petersburg
EKP	265,95	61,11	Eskimo Point
NUR	24,65	60,5	Nurmajarvi
UPS	17,35	59,9	Uppsala
T38	224,8	61	White Horse
GRK	29,38	60,27	Gorkovskaya
T46	282,71	60,05	Puvirnituq
T53	281,85	60,82	Akulivik
YKC	245,52	62,48	Yellowknife
FSP	238,77	61,76	Fort Simpson
MEK	30,97	62,77	Mekrijaervi
FHB	310,32	62	Paamiut
NAQ	314,56	61,16	Narssarssuaq
RAN	267,89	62,82	Rankine Inlet
DOB	9,11	62,07	Dombas
SOL	4,84	61,08	Solund
HAN	26,65	62,3	Hankasalmi
GAK	214,7	62,3	Gakona
FAR	352,98	62,05	Faroes
HLM	210,13	61,24	HLMS
TLK	209,9	63,3	Talkeetna
TRP	150,24	62,67	Trapper
T39	209,8	62,3	Trapper Creek
T47	284,35	62,2	Salluit
BLC	263,97	64,33	Baker Lake
LRV	338,3	64,18	Leirvogur
GHB	308,27	64,17	Nuuk
DAW	220,89	64,05	Dawson City
IQA	291,48	63,75	Iqaluit
HLL	339,44	63,77	Hella
S01	13,36	64,37	Nordli
T35	249,1	63,6	Snap Lake
T40	204,4	63	McGrath
SKT	307,1	65,42	Maniitsoq
AMK	322,37	65,6	Tasiilaq
RVK	10,99	64,94	Roervik
LYC	18,75	64,61	Lycksele
OUJ	27,23	64,52	Oulujarvi
EAG	218,8	64,78	Eagle
CMO	212,14	64,87	College
CGO	212,14	64,87	College Intl Geophys Observatory
PKR	212,74	65,08	Poker Flat
ARK	40,5	64,6	Arkhangelsk
CDC	283,4	64,2	Cape Dorset
CHC	276,8	64,1	Coral Harbour
OUL	25,85	65,1	Oulu
C07	233,31	65,26	Norman Wells
T34	249,3	64,7	Ekati
MCR	313,71	66,48	Magic2 Raven
CNL	248,75	65,75	Contwoyto Lake
JCK	16,98	66,4	Jaeckvik
DON	12,5	66,11	Doenna
CWE	190,17	66,17	Wellen
ZYK	150,78	65,75	Zyryanka
ZGN	123,26	66,75	Zhigansk
PGC	294,2	66,1	Pangnirtung
RPB	273,8	66,5	Repulse Bay
ATU	306,43	67,93	Attu
STF	309,28	67,02	Kangerlussuaq
SOD	26,63	67,37	Sodankyla
PEL	24,08	66,9	Pello
KOT	197,4	66,88	Kotzebue
BET	208,32	66,9	Bettles
FYU	214,7	66,57	Fort Yukon
T41	199,6	67	Kiana
CBB	255	69,1	Cambridge Bay
ARC	214,43	68,13	Arctic Village
INK	226,7	68,25	Inuvik
GDH	306,47	69,25	Godhavn
AND	16,03	69,3	Andenes
CPS	180,55	68,88	Cape Schmidt
CKA	73,6	68,5	Cape Kamenniy
LOP	33,08	68,25	Lop
DED	211,21	70,36	Deadhorse
NOK	88,1	69,4	NOK
UMQ	307,87	70,68	Uummannaq
SCO	338,03	70,48	Ittoqqortoormiit
TAL	266,45	69,54	Taloyoak
ALT	22,96	69,86	Alta
KEV	27,01	69,76	Kevo
MAS	23,7	69,46	Masi
KIL	20,79	69,02	Kilpisjarvi
KAU	23,05	69,02	Kautokeino
IVA	27,29	68,56	Ivalo
ABK	18,82	68,35	Abisko
LEK	13,54	68,13	Leknes
MUO	23,53	68,02	Muonio
LOZ	35,08	67,97	Lovozero
KIR	20,42	67,84	Kiruna
KAV	216,63	70,07	Kaktovik
NOR	25,79	71,09	Nordkapp
JAN	351,3	70,9	Jan Mayen
SOR	22,22	70,54	Soeroeya
TRO	18,94	69,66	Tromso
CPY	235,3	70,2	Cape Perry
AMD	61,4	69,5	Amderma
GHC	264,1	68,6	Gjoa Haven
BRW	203,25	71,3	Barrow
JCO	211,2	70,36	Jim Carrigan
TIK	129	71,58	Tixie
CHD	147,89	70,62	Chokurdakh
PBK	170,9	70,1	Pebek
IGC	278,2	69,3	Igloolik
PBC	270,3	68,5	Pelly Bay
CY0	291,4	70,5	Clyde River
UPN	303,85	72,78	Upernavik
MCE	326,1	72,4	Magic1 east
MCW	317,41	72	Magic1 west
MCG	321,65	72,6	Magic2 GISP
SUM	321,7	72,3	Summit
SAH	234,77	71,98	Sachs Harbour
DIK	80,57	73,55	Dixon
RES	265,11	74,69	Resolute Bay
KUV	302,82	74,57	Kullorsuaq
DNB	339,78	74,3	Daneborg
MCN	322,38	73,93	Magic1 north
BJN	19,2	74,5	Bear Island
TAB	291,18	76,54	Thule Air Base
SVS	294,9	76,02	Savissivik
KTN	137,71	75,94	Kotelnyy
MBC	240,64	76,32	Mould Bay
THL	290,77	77,47	Qaanag
DMH	341,37	76,77	Danmarkshavn
HOP	25,01	76,51	Hopen Island
HRN	15,6	77	Hornsund
CCS	104,28	77,72	Chelyuskin
HOR	15,6	77	Hornsund
NAL	11,95	78,92	New Aalesund
LYR	15,83	78,2	Longyearbyen
BBG	14,23	78,07	Barentsburg
VIZ	76,98	79,48	Vieze Island
HIS	58,05	80,62	Heiss Island
EUA	274,1	80	Eureka
ALE	297,5	82,5	Alert
NRD	343,33	81,6	Nord
