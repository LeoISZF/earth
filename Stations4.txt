IAGA	GLON	GLAT	STATION-NAME
A06	355,87	5,33	Abidjan
ABK	18,82	68,35	Abisko
A01	7,39	8,99	Abuja
AAE	38,77	9,03	Addis Ababa
ADL	138,65	-34,67	Adelaide
T53	281,85	60,82	Akulivik
JRS	-78,3	0	Alem Ecua
ALE	297,5	82,5	Alert
A02	2,93	36,85	Algeria
ABG	72,87	18,62	Alibag
ASP	133,88	-23,77	Alice Springs
AAA	76,92	43,25	Alma Ata
ALT	22,96	69,86	Alta
AMA	129,33	28,17	Amami-Oshima
AMD	61,4	69,5	Amderma
ASA	170,7	-14,28	American Samoa
M05	263,7	38,5	Americus
AMU	210,1	61,2	Anchorage
ANC	282,85	-11,77	Ancon
AND	16,03	69,3	Andenes
C01	276,1	42,42	Ann Arbor
TAN	47,55	-18,92	Antananarivo
PG1	77,2	-85,5	Antartica
PG2	57,96	-84,42	Antartica
PG3	37,62	-84,42	Antartica
PG4	12,2	-83,34	Antartica
ANT	289,76	-23,39	Antofagasta
API	188,22	-13,8	Apia
APL	283,12	39,17	Applied Physics Lab
ARC	214,43	68,13	Arctic Village
ARK	40,5	64,6	Arkhangelsk
ARS	58,57	56,43	Arti
ASC	345,62	-7,95	Ascension Island
ASB	142,17	43,46	Ashibetu
ASH	58,1	37,95	Ashkabad
ASW	32,51	23,5	Aswan
T36	246,69	54,71	Athabasca
ATU	306,43	67,93	Attu
BCL	105,71	9,3	Bac_Lieu
C02	265,79	57,71	Back Lake
BLC	263,97	64,33	Baker Lake
A12	100,61	14,08	Bangkok
BNG	18,57	4,33	Bangui
BGY	35,09	31,72	Bar Gyora
BBG	14,23	78,07	Barentsburg
BOX	38,97	58,03	Barok
BRW	203,25	71,3	Barrow
T15	275,66	46,24	Bay Mills
BSL	270,37	30,35	Bay St Louis
BJN	19,2	74,5	Bear Island
BJI	116,18	40,06	Beijing
BMT	116,2	40,3	Beijing Ming Tombs
A11	311,55	-1,45	Belem
BEL	20,8	51,83	Belsk
BKL	102,31	-3,86	Bengkulu
M04	263,84	41,36	Bennington
BET	208,32	66,9	Bettles
BIK	136,05	-1,08	Biak
BSV	139,21	-25,54	Birdsville
BRZ	24,75	56,21	Birzai
BFO	8,33	48,33	Black Forest
BOR	38,33	58,03	Borok
BOU	254,77	40,13	Boulder
BRD	260,03	49,87	Brandon
BFE	11,67	55,62	Brorfelde
BDV	14,02	49,07	Budkov
FIT	279,05	28,07	Bullcreek
CDO	124,63	8,4	CagayanDeOro
M02	266,75	45,56	Cambridge
CBB	255	69,1	Cambridge Bay
CMD	150,67	-34,06	Camden
A03	11,52	3,87	Cameroon
CAN	149,36	-35,3	Canberra
CNB	150,7	-34,1	Canberra
CDC	283,4	64,2	Cape Dorset
CKA	73,6	68,5	Cape Kamenniy
CPY	235,3	70,2	Cape Perry
CPS	180,55	68,88	Cape Schmidt
T16	240,2	39,19	Carson City
CSY	110,53	-66,28	Casey
CST	11,65	46,05	Castello Tesino
CEB	123,91	10,36	Cebu
CLF	2,27	48,02	Chambon la foret
CNH	124,86	44,08	Changchun
CTA	146,3	-20,1	Charters Towers
CCS	104,28	77,72	Chelyuskin
CDP	256,3	31	Chengdu
CYG	126,85	36,37	Cheongyang
T30	285,6	49,8	Chibougamau
CBI	142,3	27,15	Chichijima
CRP	275,09	10,44	Chiripa
CHD	147,89	70,62	Chokurdakh
CLK	285	44,7	Clarkson
CY0	291,4	70,5	Clyde River
CKI	96,84	-12,19	Cocos-Keeling Islands
CMO	212,14	64,87	College
CGO	212,14	64,87	College Intl Geophys Observatory
A07	346,34	9,58	Conakry
WIC	15,52	47,55	Conrad Observatorium
CNL	248,75	65,75	Contwoyto Lake
CKT	145,25	-15,48	Cooktown
CHC	276,8	64,1	Coral Harbour
CRK	357,36	57,09	Crooktree
CUL	149,58	-30,28	Culgoora
DES	39,12	-6,46	Dal Es Salaam
DLT	108,48	11,94	Dalat
DAL	151,2	-27,18	Dalby
DNB	339,78	74,3	Daneborg
DMH	341,37	76,77	Danmarkshavn
DSO	278,6	36,25	Darsky
DRW	130,9	-12,4	Darwin
A08	125,4	7	Davao
DVS	77,97	-68,58	Davis
DAW	220,89	64,05	Dawson City
DED	211,21	70,36	Deadhorse
DLR	259,08	29,49	Del Rio
T17	287,87	44,95	Derby
E04	23,93	38,08	Dionyssos
DIK	80,57	73,55	Dixon
DON	12,5	66,11	Doenna
DOB	9,11	62,07	Dombas
DMC	124,17	-75,25	Dome Concordia
DOU	4,6	50,1	Dourbes
DRV	140,01	-66,67	Dumont Durville
DRB	30,56	-29,48	Durban
DUR	14,28	41,39	Duronia
EAG	218,8	64,78	Eagle
EBR	0,49	40,82	Ebro
EDM	246,7	53,3	Edmonton
ELT	34,95	29,67	Eilat
T34	249,3	64,7	Ekati
ESA	141,36	39,24	Esashi
ESC	301,08	-62,18	Escudero
ESK	356,8	55,32	Eskdalemuir
EKP	265,95	61,11	Eskimo Point
A04	39,46	14,28	Ethiopia
EUA	274,1	80	Eureka
EUS	-38,43	-3,88	Eusebio
EWA	202	21,32	Ewa Beach
EYR	172,4	-43,4	Eyrewell
AIA	295,74	-65,25	Faraday Islands
FAR	352,98	62,05	Faroes
FYM	35,5	29,18	Fayum
T33	259,1	54	Flin Flon/The Pas
C03	248,89	58,77	Fort Chipewyan
FCC	265,92	58,76	Fort Churchill
FMC	248,79	56,66	Fort McMurray
FSP	238,77	61,76	Fort Simpson
SMI	248,05	60,02	Fort Smith
FSJ	239,27	56,23	Fort St, John
FVE	243,98	58,37	Fort Vermilion
T18	259,32	46,09	Fort Yates
FYU	214,7	66,57	Fort Yukon
FRD	282,63	38,2	Fredericksburg
FRN	240,3	37,1	Fresno
FUR	11,28	48,17	Furstenfeldbruk
GAK	214,7	62,3	Gakona
GAN	73,15	0,69	Gan
GIM	265,36	56,38	Gillam
GNG	115,71	-31,36	Gingin
GHC	264,1	68,6	Gjoa Haven
GML	356,32	57,16	Gleenmore Lodge
GLN	262,88	49,65	Glenlea
M01	263,55	46,87	Glyndon
GNA	116	-31,8	Gnangara
GDH	306,47	69,25	Godhavn
T28	299,5	53,3	Goose Bay
GRK	29,38	60,27	Gorkovskaya
GTF	288,05	43,62	Grafton
GCK	20,77	44,63	Grocka
GUA	144,87	13,59	Guam
GZH	113,34	23,09	Guangzhou
GUI	343,57	28,32	Guimar
C04	251,74	50,06	Gull Lake
GSI	97,34	1,18	Gunung Sitoli
HAN	26,65	62,3	Hankasalmi
HBK	27,71	-25,88	Hartebeesthoek
HAD	355,52	50,98	Hartland
HTY	139,8	33,12	Hatizyo
HIS	58,05	80,62	Heiss Island
HLP	18,82	54,61	Hel
HLL	339,44	63,77	Hella
HER	19,23	-34,43	Hermanus
HLM	210,13	61,24	HLMS
HOB	147,35	-42,88	Hobart
HOM	209,53	59,7	Homer
HON	202	21,32	Honolulu
HOP	25,01	76,51	Hopen Island
HRN	15,6	77	Hornsund
HOR	15,6	77	Hornsund
T19	245,33	47,61	Hot Springs
HLN	121,55	23,9	Hualien
HUA	284,67	-12,05	Huancayo
HRB	18,19	47,86	Hurbanovo
HYB	78,6	17,4	Hyderabad
ICA	-75,75	-14,08	Ica
IGC	278,2	69,3	Igloolik
ILR	4,68	8,5	Ilorin
T44	281,95	58,47	Inukjuak
INK	226,7	68,25	Inuvik
IQA	291,48	63,75	Iqaluit
IRT	104,45	52,17	Irkoutsk
IPM	250,58	-27,2	Isla de Pascua
ISL	265,34	53,86	Island Lake
SCO	338,03	70,48	Ittoqqortoormiit
IVA	27,29	68,56	Ivalo
IZN	29,73	40,5	Iznik
JAX	278,4	30,35	Jacksonville
JCK	16,98	66,4	Jaeckvik
JAI	75,8	26,92	Jaipur
JAN	351,3	70,9	Jan Mayen
JYP	140,7	2,51	Jayapura
SBH	116,07	6,02	Jayapura
JCO	211,2	70,36	Jim Carrigan
M11	256,9	20,7	Juriquilla
KAG	130,72	31,48	Kagoshima
KDU	132,47	-12,69	Kakadu
KAK	140,18	36,23	Kakioka
KAV	216,63	70,07	Kaktovik
KLD	20,2	54,5	Kaliningrad
ISK	29,06	41,07	Kandilli
STF	309,28	67,02	Kangerlussuaq
KNY	130,88	31,42	Kanoya
T32	277,7	49,4	Kapuskasing
KGD	73,08	49,82	Karaganda
KAR	5,24	59,21	Karmoey 
KAT	117,62	-33,68	Katanning
KAU	23,05	69,02	Kautokeino
KMH	18,11	-26,54	Keetmanshoop
KEV	27,01	69,76	Kevo
KHB	134,69	47,61	Khabarovsk
KRT	13,02	33,37	Khartoum
HVD	91,67	48,01	Khovd
T41	199,6	67	Kiana
KIV	30,3	50,72	Kiev
KIL	20,79	69,02	Kilpisjarvi
E03	24,1	38,63	Kimi
KEP	323,51	-54,28	King Edward Point
KIR	20,42	67,84	Kiruna
E02	21,97	39,45	Klokotos
KOR	134,5	7,33	Koror
KTN	137,71	75,94	Kotelnyy
KTB	100,32	-0,2	Kototabang
KOT	197,4	66,88	Kotzebue
KOU	307,27	5,21	Kourou
KUJ	131,23	33,06	Kuju
KUV	302,82	74,57	Kullorsuaq
KPG	123,4	-10,2	Kupang
T29	291,8	58,3	Kuujjuaq
T45	282,24	55,28	Kuujjuarapik
KVI	17,63	59,5	Kvistaberg
T42	254,84	55,15	La Ronge
SER	288,87	-30	La Serena
LAG	3,27	6,48	Lagos
LAN	357,23	54,01	Lancaster
LKW	99,78	6,3	Langkawi
LZH	103,85	36,09	Lanzhou
AQU	13,32	42,38	LAquila
LRM	114,1	-22,22	Learmonth
LGZ	123,74	13,1	Legazpi
LRV	338,3	64,18	Leirvogur
LEK	13,54	68,13	Leknes
LEM	147,5	-42,3	Lemont
LNN	30,7	59,95	Leningrad
LER	358,82	60,13	Lerwick
LET	247,13	49,64	Lethbridge
M10	260,4	24,8	Linares
C05	264,54	52,03	Little Grand Rapids
LIV	299,61	-62,66	Livingston Island
LWA	104,06	-5,02	Liwa
LYR	15,83	78,2	Longyearbyen
LON	16,66	45,41	Lonjsko Polje
P01	16,66	45,41	Lonjsko Polje
LOP	33,08	68,25	Lop
CER	289,4	-33,45	Los Cerrillos
LOV	17,83	59,35	Lovo
LOZ	35,08	67,97	Lovozero
T20	281,62	40,18	Loysburg
LNP	121,17	25	Lunping
LSK	28,19	-15,23	Lusaka
LVV	23,75	49,9	Lvov
LYC	18,75	64,61	Lycksele
M09	262,2	26,4	Lyford
B01	296,52	-64,82	m65-297
B02	294	-66,03	m66-294
B03	291,88	-67,57	m67-292
B04	41,08	-68,58	m68-041
B05	41,32	-69,29	m69-041
B06	39,4	-69,67	m70-039
B07	44,28	-70,7	m70-044
B24	77,77	-71,67	m72-078
B08	159,03	-72,77	m73-159
B09	42,99	-74,01	m74-043
B10	39,71	-77,32	m77-040
B11	336,58	-77,51	m78-337
B12	335,88	-79,08	m79-336
B13	77	-80	m80-077
B27	3	-81	m81-003
B14	337,74	-80,89	m81-338
B15	2,97	-81,49	m82-003
B16	347,06	-82,78	m83-347
B17	347,76	-82,9	m83-348
B18	336,14	-84,35	m84-336
B19	2,06	-85,36	m85-002
B20	95,98	-85,36	m85-096
B21	28,41	-87	m87-028
B22	68,17	-86,51	m87-069
B23	316,13	-88,03	m88-316
MCQ	158,95	-54,5	Macquarie Island
MGD	150,86	59,97	Magadan
MCE	326,1	72,4	Magic1 east
MCN	322,38	73,93	Magic1 north
MCW	317,41	72	Magic1 west
MCG	321,65	72,6	Magic2 GISP
MCR	313,71	66,48	Magic2 Raven
MND	124,84	1,44	Manado Indon
MAB	5,68	50,3	Manhay
SKT	307,1	65,42	Maniitsoq
A09	120	14,58	Manila
MZH	117,4	49,6	Manzaoli
LMM	40	-25,92	Maputo
AMS	77,57	-37,8	Martin de Vivias
MAS	23,7	69,46	Masi
MAW	62,88	-67,61	Mawson
MBO	343,03	14,38	Mbour
T40	204,4	63	McGrath
MCM	166,67	-77,85	McMurdo Station
MEA	246,65	54,62	Meanook
MEK	30,97	62,77	Mekrijaervi
MLB	145,18	-38,36	Melbourne
MMB	144,19	43,91	Memambetsu
MID	182,62	28,21	Midway
MSH	288,52	42,6	Millstone Hill
C06	247,03	53,35	Ministik Lake
MNK	26,52	54,1	Minsk
MIR	93,02	-66,55	Mirny
MLT	30,89	29,52	Misallat
MIZ	141,2	39,11	Mizusawa
MOS	37,32	55,48	Moscow
MSR	142,27	44,37	Moshiri
MBC	240,64	76,32	Mould Bay
MSK	34,52	52,69	MSK
MUT	121,02	14,37	Muntinlupa
MUO	23,53	68,02	Muonio
NCK	16,72	47,63	Nagycenk
NAN	298,3	56,4	Nain
NAB	36,48	-1,16	Nairobi
A05	17,58	-19,2	Namibia
NMP	39,25	-15,09	Nampula
NAQ	314,56	61,16	Narssarssuaq
VNA	351,7	-70,7	Neumayer Station III
E01	23,86	41,35	Nevrokopi
NAL	11,95	78,92	New Aalesund
NEW	242,88	48,27	Newport
NGK	12,68	52,07	Niemegk
NOK	88,1	69,4	NOK
NRD	343,33	81,6	Nord
NOR	25,79	71,09	Nordkapp
S01	13,36	64,37	Nordli
C07	233,31	65,26	Norman Wells
NKK	62,12	45,77	Novokazalinsk
NVL	11,83	-70,78	Novolazorevskaya
NVS	82,9	55,03	Novosibirsk
NUR	24,65	60,5	Nurmajarvi
GHB	308,27	64,17	Nuuk
ODE	30,88	46,22	Odessa
OHI	302,1	-63,32	OHiggins
ONW	141,47	38,43	Onagawa
ORC	315,26	-60,74	Orcadas
C08	264,92	45,87	Osakis
OSO	286,91	-40,34	Osorno
OTT	284,45	45,4	Ottawa
OUL	25,85	65,1	Oulu
OUJ	27,23	64,52	Oulujarvi
C09	264,71	54,93	Oxford House
FHB	310,32	62	Paamiut
PAL	295,95	-64,77	Palmer
PPT	210,42	-17,57	Pamatai
PAG	24,18	47,49	Panagjurishte
PGC	294,2	66,1	Pangnirtung
PNL	303,82	16,68	Pantanal
PET	158,25	52,97	Paratunka
PRP	119,4	-3,6	ParePare
PBK	170,9	70,1	Pebek
PEG	23,9	38,1	Pedeli
PEL	24,08	66,9	Pello
PBC	270,3	68,5	Pelly Bay
T22	226,84	56,83	Petersburg
A10	319,5	-9,4	Petrolina
A13	98,35	7,89	Phuket
PHU	105,95	21,03	Phuthuy
PIL	294,47	-31,4	Pilar
PIN	263,96	50,2	Pinawa
T21	257,41	43,08	Pine Ridge
PKR	212,74	65,08	Poker Flat
C10	245,79	47,66	Polson
PTN	109,25	-0,05	Pontianak
PPI	131,73	42,98	Popov Island
CZT	51,87	-46,43	Port Alfred
PAF	70,26	-49,35	Port aux Francias
PST	302,11	-51,7	Port Stanley
PBQ	282,26	55,28	Poste de la Baleine
T43	245,7	50,9	Priddis
T37	237,2	53,8	Prince George
PRG	76,23	-69,23	Progress
PNT	289,1	-53,2	Puerto Natales
PAC	289,91	-40,34	Punta Arenas
M06	262,6	35	Purcell
PUT	290,5	-18,33	Putre
T46	282,71	60,05	Puvirnituq
THL	290,77	77,47	Qaanag
QSB	35,64	33,87	Qsaybeh
RAL	256,32	58,22	Rabbit Lake
T52	282,38	53,79	Radisson
RNC	12,08	43,97	Ranchio
RAN	267,89	62,82	Rankine Inlet
RED	246,16	52,14	Red Deer
T23	274,86	43,66	Remus
RPB	273,8	66,5	Repulse Bay
RES	265,11	74,69	Resolute Bay
M07	262,25	32,98	Richardson
RIK	143,76	43,48	Rikubetsu
ROC	150,31	-23,19	Rockhampton
ROE	8,55	55,17	Roemoe
RVK	10,99	64,94	Roervik
ROT	245,87	51,07	Rothney
RSV	12,45	55,85	Rude Skov
SBL	299,99	43,93	Sable Carrigan
SAH	234,77	71,98	Sachs Harbour
SPG	29,72	60,54	Saint Petersburg
T50	287,55	48,65	Saint-Fellicien
T47	284,35	62,2	Salluit
M08	261,39	29,44	San Antonio
SFS	354,06	36,67	San Fernando
T26	242,15	34,2	San Gabriel
ENP	289,1	-52,13	San Gregorio
SJG	293,85	18,11	San Juan
SPT	355,65	39,55	San Pablo Toledo
T31	280,8	56,5	Sanikiluaq
G01	306,28	-29,72	Santa Maria
SMA	334,87	36,99	Santa Maria/Azoren
SAS	253,6	52,8	Saskatoon
SVS	294,9	76,02	Savissivik
T48	293,19	54,8	Schefferville
SBA	166,78	-77,85	Scott Base
T24	271,4	44,78	Shawano
SHU	199,54	55,38	Shumagin
SCN	100,3	-0,55	Sicincin
SIT	224,67	57,07	Sitka
T35	249,1	63,6	Snap Lake
SOD	26,63	67,37	Sodankyla
SOR	22,22	70,54	Soeroeya
SOL	4,84	61,08	Solund
SON	66,44	25,12	Sonmiani
SPA	0	-90	South Pole Station
T49	293,73	50,22	Spet-lles
STJ	307,32	47,6	St Johns
SHE	354,27	-15,95	St, Helena
PTK	158,25	52,94	St,Paratunka
SUM	321,7	72,3	Summit
SUA	26,25	45,32	Surlari
SUT	20,67	-32,4	Sutherland
SUW	23,18	54,01	Suwalki
SZC	19,61	52,91	Szczechowo
T27	242,32	34,38	Table Mountain
TLK	209,9	63,3	Talkeetna
TAL	266,45	69,54	Taloyoak
TAM	5,53	22,79	Tamanrasset
TAR	26,46	58,26	Tartu
TKT	69,62	41,33	Tashkent
AMK	322,37	65,6	Tasiilaq
TEO	260,82	19,75	Teoloyucan
C11	263,64	48,03	Thief River Falls
TAB	291,18	76,54	Thule Air Base
THY	17,54	46,9	Tihany
TIR	76,95	8,48	Tirunelveli
TIK	129	71,58	Tixie
TOL	355,95	39,88	Toledo
TND	124,95	1,29	Tondano
TWN	146,82	-19,27	Townsville
TRP	150,24	62,67	Trapper
T39	209,8	62,3	Trapper Creek
TRW	294,68	-43,25	Trelew
TDC	347,69	-37,1	Tristan de Cunha
TRO	18,94	69,66	Tromso
TSU	17,7	-19,22	Tsumeb
TUC	249,27	32,17	Tucson
TGG	121,76	17,66	Tuguegarao
TUL	264,22	35,92	Tulsa
USC	278,54	33,34	U of South Carolina
T25	241,07	45,14	Ukiah
UPN	303,85	72,78	Upernavik
UPS	17,35	59,9	Uppsala
WMQ	87,71	43,81	Urumqi
UMQ	307,87	70,68	Uummannaq
VLD	286,86	-39,48	Valdivia
VLO	282,24	48,19	Val-dOr
T51	282,22	48,05	Val-dOr
VAL	349,75	51,93	Valentia
VSS	316,35	-22,4	Vassouras
E05	22,95	36,72	Velies
VIC	236,58	48,52	Victoria
VIZ	76,98	79,48	Vieze Island
VRE	292,38	-17,28	Villa Remedios
VOS	106,87	-78,45	Vostok
T03	247,02	50,37	Vulcan
VYH	18,84	48,49	Vyhne
WAD	356,1	51,9	Wadena
WTK	112,63	-7,56	Watukosek
WEP	141,88	-12,68	Weipa
CWE	190,17	66,17	Wellen
C13	239,97	51,88	Wells Gray
WEW	143,62	-3,55	Wewak
C12	256,2	49,69	Weyburn
T38	224,8	61	White Horse
WHS	264,75	49,8	Whiteshell
WNG	9,07	53,75	Wingst
M03	264,4	43,6	Worthington
YAK	129,72	60,02	Yakutsk
YMK	130,62	31,19	Yamakawa
YAP	138,5	9,3	Yap
YKC	245,52	62,48	Yellowknife
YOR	358,95	53,95	York
YSS	142,72	46,95	Yuzhno Sakhalinsk
ZAG	20,58	50,28	Zagorzyce
KNZ	48,85	55,83	Zaymishche
ZGN	123,26	66,75	Zhigansk
ZYK	150,78	65,75	Zyryanka
